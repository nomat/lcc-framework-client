import { EXTENSION_PATH, exeCommand, findFile, FindFileType, getExtensionConfig,getFrameworkConfig,mapDirectory,copyFile } from '../utils';
import { loadConfig } from '../config';
import { FrameworkConfigurator } from './framework-config'

const Fs = require('fs');
const Path = require('path');

const econfig = getExtensionConfig();

/**
 * 构建开始
 */
export async function onBuildStart(options:BuildOptions, callback:Function){
	callback();
}

/**
 * 构建完成
 */
export async function onBuildFinished(options:BuildOptions, callback:Function){
	//Editor.log(`${TAG} onBuildFinished ${JSON.stringify(options)}`);
	let pkgOptions = loadConfig().publish;
	let destJs = findFile(options.dest, `${econfig.name}.js`, FindFileType.FILE);
	if(destJs){
        const VERSION = getExtensionConfig().version;
        const tempPath = Path.join(Editor.Project.path, 'temp', econfig.name);
        if(!Fs.existsSync(tempPath)){
            Fs.mkdirSync(tempPath, { recursive : true });
        }
		let modules = '';
		let fconfig = getFrameworkConfig();
		if(pkgOptions.clip){
			if(pkgOptions.clipMethod == 'auto'){
				let pathList:string[] = [];
				mapDirectory(Path.join(Editor.Project.path, 'assets'), (path, file, isdir)=>{
					if(!isdir && file != `${econfig.name}.js` && file != `${econfig.name}.d.ts` &&
						(path.endsWith('.ts') || 
						path.endsWith('.js') || 
						path.endsWith('.scene') || 
						path.endsWith('.prefab'))){
						pathList.push(path);
					}
					return false;
				});
				const autoclipCacheConfigFile = Path.join(tempPath, 'autoclip-cache-config.json');
				let autoclipCacheConfig:{ version:number , filetimes:Record<string,number>, modules:string } = { version:VERSION, filetimes:{}, modules:"" };
				do {
					// 检查缓存
					if(Fs.existsSync(autoclipCacheConfigFile)){
						autoclipCacheConfig = JSON.parse(Fs.readFileSync(autoclipCacheConfigFile, { encoding : 'utf-8' }));
						if(Object.keys(autoclipCacheConfig.filetimes).length == pathList.length && autoclipCacheConfig.version == VERSION){
							if((()=>{
								for(let _path of pathList){
									let pinfo = Fs.statSync(_path);
									if(Math.floor(pinfo.mtimeMs) != autoclipCacheConfig.filetimes[_path]){
										return false;
									}
								}
								return true;
							})()){
								Editor.log(`${econfig.name} 使用自动裁剪缓存`);
								modules = autoclipCacheConfig.modules;
								break;
							}
						}
					}

					// 重新构建缓存
					if(Fs.existsSync(autoclipCacheConfigFile)){
						Fs.unlinkSync(autoclipCacheConfigFile);
					}
					autoclipCacheConfig.version = VERSION;
					autoclipCacheConfig.filetimes = {};
					let autoclipCache = '';
					for(let _path of pathList){
						let pinfo = Fs.statSync(_path);
						autoclipCacheConfig.filetimes[_path] = Math.floor(pinfo.mtimeMs);
						autoclipCache += Fs.readFileSync(_path, { encoding : 'utf-8' });
					}
					let fconfigor = new FrameworkConfigurator(fconfig);
					for(let mname in fconfig.modules){
						let config = fconfig.modules[mname];
						if(config.keywords){
							for(let keyword of config.keywords){
								let reg = new RegExp(`[^a-zA-Z_$]${keyword}[^a-zA-Z_$]`);
								if(reg.test(autoclipCache)){
									Editor.log(`${econfig.name} KEYWORD '${keyword}' MODULE '${mname}'`);
									fconfigor.keepModule(mname);
									break;
								}
							}
						}
					}
					autoclipCacheConfig.modules = fconfigor.collectModules().filter((mname:string)=>{ return !fconfig.modules[mname].baseModule; }).join(',');
					modules = autoclipCacheConfig.modules;
					Fs.writeFileSync(autoclipCacheConfigFile, JSON.stringify(autoclipCacheConfig));
				}while(false);
			}else{
				let keepModules = pkgOptions.keepModules;
				modules = Object.keys(fconfig.modules).filter((mname:string)=>{ 
					let module = fconfig.modules[mname];
					let keep = keepModules[mname];
					return !module.baseModule && (keep != null ? keep : module.keepDefault);
				}).join(',');
			}
		}else{
			modules = Object.keys(fconfig.modules).filter((mname:string)=>{ return !fconfig.modules[mname].baseModule; }).join(',');
		}
		
		Editor.log(`${econfig.name} 模块:${modules}`);
        
        let debug = pkgOptions.confuse ? false : options.debug;
        const option_debug = debug?'--debug':'';
        const option_modules = modules?`--modules "${modules}"`:'';
        const option_confuse = pkgOptions.confuse?'--confuse':'';

        const frameworkCacheConfigFile = Path.join(tempPath, 'framework-cache-config.json');
        const frameworkCacheFile = Path.join(tempPath, 'framework-cache.js');
        let frameworkCacheConfig:{ version:number ,options:Record<string,string> } = { version:VERSION, options:{} };
        do {
            // 检查缓存
            if(Fs.existsSync(frameworkCacheConfigFile) && Fs.existsSync(frameworkCacheFile)){
                frameworkCacheConfig = JSON.parse(Fs.readFileSync(frameworkCacheConfigFile, { encoding : 'utf-8' }));
                if(frameworkCacheConfig.version == VERSION && 
                    frameworkCacheConfig.options['debug'] == option_debug && 
                    frameworkCacheConfig.options['confuse'] == option_confuse && 
                    frameworkCacheConfig.options['modules'] == option_modules){
						Editor.log(`${econfig.name} 使用框架缓存`);
                    break;
                }
            }

			// 重新编译框架
			if(Fs.existsSync(frameworkCacheConfigFile)){
				Fs.unlinkSync(frameworkCacheConfigFile);
			}
			if(Fs.existsSync(frameworkCacheFile)){
				Fs.unlinkSync(frameworkCacheFile);
			}
            let command = `npx gulp buildPublishFramework --outDir "${tempPath}" --outFile framework-cache.js ${option_debug} ${option_confuse} ${option_modules}`;
            //Editor.log(`${TAG} onBuildFinished ${command}`);
			let result = await exeCommand(command, EXTENSION_PATH);
            if(result.error){
                Editor.error('框架编译失败: ' + result.error);
                break;
            }else{
                // 写入缓存
                frameworkCacheConfig.version = VERSION;
                frameworkCacheConfig.options['debug'] = option_debug;
                frameworkCacheConfig.options['confuse'] = option_confuse;
                frameworkCacheConfig.options['modules'] = option_modules;
                Fs.writeFileSync(frameworkCacheConfigFile, JSON.stringify(frameworkCacheConfig));
            }
        }while(false);

        if(Fs.existsSync(frameworkCacheFile)){
            copyFile(frameworkCacheFile, destJs);
			Editor.log(`${econfig.name} ${econfig.name}.js 替换成功`);
        }
	}
	callback();
}
