//import { EXTENSION_PATH, getGamePacks, exeCommand, getFrameworkConfig } from '../utils';
//import { loadConfig, saveConfig } from '../config';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const { EXTENSION_PATH, getGamePacks, exeCommand, getFrameworkConfig, getExtensionConfig } = require(`${Editor.Project.path}/packages/lcc-framework-client/bin/utils`);
const { loadConfig, saveConfig } = require(`${Editor.Project.path}/packages/lcc-framework-client/bin/config`);
const OS = require('os');
const Fs = require('fs');
const Path = require('path');
const econfig = getExtensionConfig();
/**
 * 收集模块
 */
function collectModules() {
    let keepModules = {};
    let moduleDescs = {};
    let config = loadConfig();
    const fconfig = getFrameworkConfig();
    for (let mname in fconfig.modules) {
        let module = fconfig.modules[mname];
        if (mname.indexOf("base") != 0) {
            let keep = config.publish.keepModules[mname];
            moduleDescs[mname] = module.description;
            keepModules[mname] = keep != null ? keep : module.keepDefault;
        }
    }
    return {
        keeps: keepModules,
        descs: moduleDescs,
    };
}
Editor.Panel.extend({
    style: Fs.readFileSync(Path.join(EXTENSION_PATH, 'htmls', 'fontello.css'), { encoding: 'utf-8' }) +
        Fs.readFileSync(Path.join(EXTENSION_PATH, 'htmls', 'panel-frame.css'), { encoding: 'utf-8' }) +
        Fs.readFileSync(Path.join(EXTENSION_PATH, 'htmls', 'functions.css'), { encoding: 'utf-8' }),
    template: `
<div id="functions" loaded="">
	<div class="slider">
		<div v-bind:active="showpage==0" v-on:click="changePage(0)">${Editor.T(econfig.name + '.label-build-pack-declaration')}</div>
		<div v-bind:active="showpage==1" v-on:click="changePage(1)">${Editor.T(econfig.name + '.label-publish-setting')}</div>
	</div>
	<div class="tabs" v-show="showpage==0">
		<header>
			<h1>${Editor.T(econfig.name + '.label-build-pack-declaration')}</h1>
		</header>
		<section>
			<div class="scroll">
				<style type="text/css">.fit,.fixed-bottom,.fixed-top{right:0}.fit,.fixed-bottom,.fixed-left{bottom:0;left:0}.layout.horizontal,.layout.horizontal-reverse,.layout.vertical,.layout.vertical-reverse{display:-ms-flexbox;display:-webkit-flex;display:flex}.layout.inline{display:-ms-inline-flexbox;display:-webkit-inline-flex;display:inline-flex}.layout.horizontal{-ms-flex-direction:row;-webkit-flex-direction:row;flex-direction:row}.layout.horizontal-reverse{-ms-flex-direction:row-reverse;-webkit-flex-direction:row-reverse;flex-direction:row-reverse}.layout.vertical{-ms-flex-direction:column;-webkit-flex-direction:column;flex-direction:column}.layout.vertical-reverse{-ms-flex-direction:column-reverse;-webkit-flex-direction:column-reverse;flex-direction:column-reverse}.layout.wrap{-ms-flex-wrap:wrap;-webkit-flex-wrap:wrap;flex-wrap:wrap}.layout.wrap-reverse{-ms-flex-wrap:wrap-reverse;-webkit-flex-wrap:wrap-reverse;flex-wrap:wrap-reverse}.flex-auto{-ms-flex:1 1 auto;-webkit-flex:1 1 auto;flex:1 1 auto}.flex-none{-ms-flex:none;-webkit-flex:none;flex:none}.flex,.flex-1{-ms-flex:1;-webkit-flex:1;flex:1}.flex-2{-ms-flex:2;-webkit-flex:2;flex:2}.flex-3{-ms-flex:3;-webkit-flex:3;flex:3}.flex-4{-ms-flex:4;-webkit-flex:4;flex:4}.flex-5{-ms-flex:5;-webkit-flex:5;flex:5}.flex-6{-ms-flex:6;-webkit-flex:6;flex:6}.flex-7{-ms-flex:7;-webkit-flex:7;flex:7}.flex-8{-ms-flex:8;-webkit-flex:8;flex:8}.flex-9{-ms-flex:9;-webkit-flex:9;flex:9}.flex-10{-ms-flex:10;-webkit-flex:10;flex:10}.flex-11{-ms-flex:11;-webkit-flex:11;flex:11}.flex-12{-ms-flex:12;-webkit-flex:12;flex:12}.layout.start{-ms-flex-align:start;-webkit-align-items:flex-start;align-items:flex-start}.layout.center,.layout.center-center{-ms-flex-align:center;-webkit-align-items:center;align-items:center}.layout.end{-ms-flex-align:end;-webkit-align-items:flex-end;align-items:flex-end}.layout.start-justified{-ms-flex-pack:start;-webkit-justify-content:flex-start;justify-content:flex-start}.layout.center-center,.layout.center-justified{-ms-flex-pack:center;-webkit-justify-content:center;justify-content:center}.layout.end-justified{-ms-flex-pack:end;-webkit-justify-content:flex-end;justify-content:flex-end}.layout.around-justified{-ms-flex-pack:around;-webkit-justify-content:space-around;justify-content:space-around}.layout.justified{-ms-flex-pack:justify;-webkit-justify-content:space-between;justify-content:space-between}.self-start{-ms-align-self:flex-start;-webkit-align-self:flex-start;align-self:flex-start}.self-center{-ms-align-self:center;-webkit-align-self:center;align-self:center}.self-end{-ms-align-self:flex-end;-webkit-align-self:flex-end;align-self:flex-end}.self-stretch{-ms-align-self:stretch;-webkit-align-self:stretch;align-self:stretch}.block{display:block}[hidden]{display:none!important}.invisible{visibility:hidden!important}.relative{position:relative}.fit{position:absolute;top:0}body.fullbleed{margin:0;height:100vh}.scroll{-webkit-overflow-scrolling:touch;overflow:auto}.fixed-bottom,.fixed-left,.fixed-right,.fixed-top{position:fixed}.fixed-top{top:0;left:0}.fixed-right{top:0;right:0;bottom:0}.fixed-left{top:0}:host{display:block;position:relative;contain:content}table{border-collapse:collapse;font-size:1em;width:100%}td,th{font-weight:400;text-align:left;padding:4px 8px}.label{width:10%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis}.wrapper{position:relative}.fill{width:100%}th ui-prop{min-width:auto}tr[hovering] .label{color:#09f}tr[selected] .label{color:#fd942b}tr[is-disabled] .label{color:#636363;cursor:default} .debug td,.debug th{border:1px solid #666} .debug tr:nth-child(even){background-color:#333}</style>
				<table class="debug">
					<tr>
						<th class="label">
							<span>${Editor.T(econfig.name + '.label-pack')}</span>
						</th>
						<th class="wrapper">
							<span>${Editor.T(econfig.name + '.label-path')}</span>
						</th>
						<th class="label">
							<span>${Editor.T(econfig.name + '.label-action')}</span>
						</th>
					</tr>
					<tr v-for="(pack,path) in packs">
						<th class="label">
							<span>{{pack}}</span>
						</th>
						<th class="wrapper">
							<span>{{path}}</span>
						</th>
						<th class="label">
							<ui-button class="green" v-disabled="building" @click="buildDeclaration(pack)">${Editor.T(econfig.name + '.button-build')}</ui-button>
						</th>
					</tr>
				</table>
				<h2>${Editor.T(econfig.name + '.label-build-output')}</h2>
				<pre v-text="buildinfo"></pre>
			</div>
		</section>
		<ui-loader class="transparent" v-show="building">${Editor.T(econfig.name + '.label-building')}</ui-loader>
	</div>
	<div class="tabs" v-show="showpage==1">
		<header>
			<h1>${Editor.T(econfig.name + '.label-publish-setting')}</h1>
		</header>
		<section>
			<div class="preview">
				<ui-prop name="${Editor.T(econfig.name + '.label-framework-confuse')}" tooltip="${Editor.T(econfig.name + '.label-framework-confuse')}">
					<ui-checkbox v-value="publish.confuse"></ui-checkbox>
				</ui-prop>
				<ui-prop name="${Editor.T(econfig.name + '.label-framework-clip')}" tooltip="${Editor.T(econfig.name + '.label-framework-clip')}">
					<ui-checkbox v-value="publish.clip"></ui-checkbox>
				</ui-prop>
				<ui-prop name="${Editor.T(econfig.name + '.label-framework-clip-method')}" tooltip="${Editor.T(econfig.name + '.label-framework-clip-method')}" v-show="publish.clip">
					<ui-select v-value="publish.clipMethod">
						<option value="auto">${Editor.T(econfig.name + '.label-auto-clip')}</option>
						<option value="custom">${Editor.T(econfig.name + '.label-custom-clip')}</option>
					</ui-select>
				</ui-prop>	
				<ui-section v-show="publish.clip&&publish.clipMethod=='custom'">
					<div slot="header">${Editor.T(econfig.name + '.label-keep-modules')}</div>
					<div style="overflow: auto;height: 200px;">
						<ui-prop v-for="(module,keep) in publish.keepModules" :key="module" :name="module" :tooltip="moduleDescs[module]">
							<ui-checkbox v-value="keep"></ui-checkbox>
						</ui-prop>
					</div>
				</ui-section>
			</div>
		</section>
		<footer>
            <ui-button class="green" v-disabled="saving" @click="savePublishConfig()">${Editor.T(econfig.name + '.button-save')}</ui-button>
        </footer>
		<ui-loader class="transparent" v-show="saving">${Editor.T(econfig.name + '.label-saving')}</ui-loader>
	</div>
</div>
`,
    ready() {
        return __awaiter(this, void 0, void 0, function* () {
            let config = loadConfig();
            let modules = collectModules();
            // @ts-ignore
            const app = new window.Vue({
                el: this.shadowRoot,
                data() {
                    return {
                        showpage: 0,
                        packs: getGamePacks(),
                        building: false,
                        buildinfo: '',
                        moduleDescs: modules.descs,
                        publish: {
                            confuse: config.publish.confuse,
                            clip: config.publish.clip,
                            clipMethod: config.publish.clipMethod,
                            keepModules: modules.keeps,
                        },
                        saving: false,
                    };
                },
                methods: {
                    changePage(page) {
                        if (this.showpage != page) {
                            this.showpage = page;
                            if (page == 0) {
                                this.packs = getGamePacks();
                            }
                            else if (page == 1) {
                                modules = collectModules();
                                this.moduleDescs = modules.descs;
                                this.publish.keepModules = modules.keeps;
                            }
                        }
                    },
                    buildDeclaration(pack) {
                        return __awaiter(this, void 0, void 0, function* () {
                            this.building = true;
                            this.buildinfo = `${Editor.T(econfig.name + '.label-pack-building')} [${pack}]`;
                            if (OS.platform() == "win32") {
                                var command = `npx gulp buildDeclaration --project ${Editor.Project.path} --pack ${pack}`;
                                let result = yield exeCommand(command, EXTENSION_PATH);
                                if (result.error) {
                                    this.buildinfo += `\nerror :\n${result.error}`;
                                }
                                else {
                                    this.buildinfo += `\nstdout :\n${result.stdout}`;
                                    if (result.stderr) {
                                        this.buildinfo += `\nstderr :\n${result.stderr}`;
                                    }
                                }
                            }
                            this.buildinfo += `\n${Editor.T(econfig.name + '.label-pack-builded')}`;
                            this.building = false;
                        });
                    },
                    savePublishConfig() {
                        return __awaiter(this, void 0, void 0, function* () {
                            this.saving = true;
                            config.publish = this.publish;
                            saveConfig(config);
                            this.saving = false;
                        });
                    }
                }
            });
        });
    }
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBhbmVscy9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQSwwRkFBMEY7QUFDMUYscURBQXFEOzs7Ozs7Ozs7O0FBRXJELE1BQU0sRUFBRSxjQUFjLEVBQUUsWUFBWSxFQUFFLFVBQVUsRUFBRSxrQkFBa0IsRUFBRSxrQkFBa0IsRUFBRSxHQUFHLE9BQU8sQ0FBQyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSwwQ0FBMEMsQ0FBQyxDQUFDO0FBQ3ZLLE1BQU0sRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLEdBQUcsT0FBTyxDQUFDLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLDJDQUEyQyxDQUFDLENBQUM7QUFFOUcsTUFBTSxFQUFFLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO0FBQ3pCLE1BQU0sRUFBRSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUN6QixNQUFNLElBQUksR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7QUFFN0IsTUFBTSxPQUFPLEdBQUcsa0JBQWtCLEVBQUUsQ0FBQztBQUVyQzs7R0FFRztBQUNILFNBQVMsY0FBYztJQUNuQixJQUFJLFdBQVcsR0FBNEIsRUFBRSxDQUFDO0lBQzlDLElBQUksV0FBVyxHQUEyQixFQUFFLENBQUM7SUFDN0MsSUFBSSxNQUFNLEdBQUcsVUFBVSxFQUFFLENBQUM7SUFFMUIsTUFBTSxPQUFPLEdBQUcsa0JBQWtCLEVBQUUsQ0FBQztJQUNyQyxLQUFJLElBQUksS0FBSyxJQUFJLE9BQU8sQ0FBQyxPQUFPLEVBQUM7UUFDN0IsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNwQyxJQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFDO1lBQzFCLElBQUksSUFBSSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzdDLFdBQVcsQ0FBQyxLQUFLLENBQUMsR0FBRyxNQUFNLENBQUMsV0FBVyxDQUFDO1lBQ3hDLFdBQVcsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUM7U0FDakU7S0FDSjtJQUVELE9BQU87UUFDSCxLQUFLLEVBQUcsV0FBVztRQUNuQixLQUFLLEVBQUcsV0FBVztLQUN0QixDQUFBO0FBQ0wsQ0FBQztBQUVELE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO0lBRWhCLEtBQUssRUFBRSxFQUFFLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLE9BQU8sRUFBRSxjQUFjLENBQUMsRUFBRSxFQUFFLFFBQVEsRUFBRyxPQUFPLEVBQUUsQ0FBQztRQUNqRyxFQUFFLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLE9BQU8sRUFBRSxpQkFBaUIsQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFHLE9BQU8sRUFBRSxDQUFDO1FBQzlGLEVBQUUsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsT0FBTyxFQUFFLGVBQWUsQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFHLE9BQU8sRUFBRSxDQUFDO0lBRTdGLFFBQVEsRUFBRTs7O2dFQUdrRCxNQUFNLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsK0JBQStCLENBQUM7Z0VBQ3hELE1BQU0sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyx3QkFBd0IsQ0FBQzs7OztTQUl4RyxNQUFNLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsK0JBQStCLENBQUM7Ozs7Ozs7O2VBUWxELE1BQU0sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxhQUFhLENBQUM7OztlQUd0QyxNQUFNLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsYUFBYSxDQUFDOzs7ZUFHdEMsTUFBTSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLGVBQWUsQ0FBQzs7Ozs7Ozs7Ozs7d0ZBV2lDLE1BQU0sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxlQUFlLENBQUM7Ozs7VUFJdEgsTUFBTSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLHFCQUFxQixDQUFDOzs7O3FEQUlILE1BQU0sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxpQkFBaUIsQ0FBQzs7OztTQUl0RixNQUFNLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsd0JBQXdCLENBQUM7Ozs7cUJBSXJDLE1BQU0sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRywwQkFBMEIsQ0FBQyxjQUFjLE1BQU0sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRywwQkFBMEIsQ0FBQzs7O3FCQUdwSCxNQUFNLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsdUJBQXVCLENBQUMsY0FBYyxNQUFNLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsdUJBQXVCLENBQUM7OztxQkFHOUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLDhCQUE4QixDQUFDLGNBQWMsTUFBTSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLDhCQUE4QixDQUFDOzs2QkFFcEgsTUFBTSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLGtCQUFrQixDQUFDOytCQUN6QyxNQUFNLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsb0JBQW9CLENBQUM7Ozs7MEJBSWxELE1BQU0sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxxQkFBcUIsQ0FBQzs7Ozs7Ozs7Ozt3RkFVZ0IsTUFBTSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLGNBQWMsQ0FBQzs7bURBRTVFLE1BQU0sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxlQUFlLENBQUM7OztDQUcxRjtJQUVTLEtBQUs7O1lBQ1AsSUFBSSxNQUFNLEdBQUcsVUFBVSxFQUFFLENBQUM7WUFDMUIsSUFBSSxPQUFPLEdBQUcsY0FBYyxFQUFFLENBQUM7WUFFL0IsYUFBYTtZQUNiLE1BQU0sR0FBRyxHQUFHLElBQUksTUFBTSxDQUFDLEdBQUcsQ0FBQztnQkFDdkIsRUFBRSxFQUFFLElBQUksQ0FBQyxVQUFVO2dCQUVuQixJQUFJO29CQUNBLE9BQU87d0JBQ0gsUUFBUSxFQUFHLENBQUM7d0JBQ1osS0FBSyxFQUFHLFlBQVksRUFBRTt3QkFDdEIsUUFBUSxFQUFHLEtBQUs7d0JBQ2hCLFNBQVMsRUFBRyxFQUFFO3dCQUNkLFdBQVcsRUFBRyxPQUFPLENBQUMsS0FBSzt3QkFDM0IsT0FBTyxFQUFHOzRCQUNOLE9BQU8sRUFBRyxNQUFNLENBQUMsT0FBTyxDQUFDLE9BQU87NEJBQ2hDLElBQUksRUFBRyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUk7NEJBQzFCLFVBQVUsRUFBRyxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQVU7NEJBQ3RDLFdBQVcsRUFBRyxPQUFPLENBQUMsS0FBSzt5QkFDOUI7d0JBQ0QsTUFBTSxFQUFHLEtBQUs7cUJBQ2pCLENBQUE7Z0JBQ0wsQ0FBQztnQkFFRCxPQUFPLEVBQUU7b0JBQ0wsVUFBVSxDQUFDLElBQVc7d0JBQ2xCLElBQUcsSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLEVBQUM7NEJBQ3JCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDOzRCQUNyQixJQUFHLElBQUksSUFBSSxDQUFDLEVBQUM7Z0NBQ1QsSUFBSSxDQUFDLEtBQUssR0FBRyxZQUFZLEVBQUUsQ0FBQzs2QkFDL0I7aUNBQUssSUFBRyxJQUFJLElBQUksQ0FBQyxFQUFDO2dDQUNmLE9BQU8sR0FBRyxjQUFjLEVBQUUsQ0FBQztnQ0FDM0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDO2dDQUNqQyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDOzZCQUM1Qzt5QkFDSjtvQkFDTCxDQUFDO29CQUVLLGdCQUFnQixDQUFDLElBQVc7OzRCQUM5QixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQzs0QkFDckIsSUFBSSxDQUFDLFNBQVMsR0FBRyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxzQkFBc0IsQ0FBQyxLQUFLLElBQUksR0FBRyxDQUFDOzRCQUNoRixJQUFHLEVBQUUsQ0FBQyxRQUFRLEVBQUUsSUFBSSxPQUFPLEVBQUM7Z0NBQ3hCLElBQUksT0FBTyxHQUFHLHVDQUF1QyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksV0FBVyxJQUFJLEVBQUUsQ0FBQztnQ0FDMUYsSUFBSSxNQUFNLEdBQUcsTUFBTSxVQUFVLENBQUMsT0FBTyxFQUFFLGNBQWMsQ0FBQyxDQUFDO2dDQUN2RCxJQUFJLE1BQU0sQ0FBQyxLQUFLLEVBQUU7b0NBQ2QsSUFBSSxDQUFDLFNBQVMsSUFBSSxjQUFjLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQztpQ0FDbEQ7cUNBQUk7b0NBQ0QsSUFBSSxDQUFDLFNBQVMsSUFBSSxlQUFlLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQztvQ0FDakQsSUFBRyxNQUFNLENBQUMsTUFBTSxFQUFDO3dDQUNiLElBQUksQ0FBQyxTQUFTLElBQUksZUFBZSxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUM7cUNBQ3BEO2lDQUNKOzZCQUNKOzRCQUNELElBQUksQ0FBQyxTQUFTLElBQUksS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQUcscUJBQXFCLENBQUMsRUFBRSxDQUFDOzRCQUN4RSxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQzt3QkFDMUIsQ0FBQztxQkFBQTtvQkFFSyxpQkFBaUI7OzRCQUNuQixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQzs0QkFDbkIsTUFBTSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDOzRCQUM5QixVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7NEJBQ25CLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO3dCQUN4QixDQUFDO3FCQUFBO2lCQUNKO2FBQ0osQ0FBQyxDQUFDO1FBQ1AsQ0FBQztLQUFBO0NBQ0osQ0FBQyxDQUFDIiwiZmlsZSI6InBhbmVscy9pbmRleC5qcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG4vL2ltcG9ydCB7IEVYVEVOU0lPTl9QQVRILCBnZXRHYW1lUGFja3MsIGV4ZUNvbW1hbmQsIGdldEZyYW1ld29ya0NvbmZpZyB9IGZyb20gJy4uL3V0aWxzJztcclxuLy9pbXBvcnQgeyBsb2FkQ29uZmlnLCBzYXZlQ29uZmlnIH0gZnJvbSAnLi4vY29uZmlnJztcclxuXHJcbmNvbnN0IHsgRVhURU5TSU9OX1BBVEgsIGdldEdhbWVQYWNrcywgZXhlQ29tbWFuZCwgZ2V0RnJhbWV3b3JrQ29uZmlnLCBnZXRFeHRlbnNpb25Db25maWcgfSA9IHJlcXVpcmUoYCR7RWRpdG9yLlByb2plY3QucGF0aH0vcGFja2FnZXMvbGNjLWZyYW1ld29yay1jbGllbnQvYmluL3V0aWxzYCk7XHJcbmNvbnN0IHsgbG9hZENvbmZpZywgc2F2ZUNvbmZpZyB9ID0gcmVxdWlyZShgJHtFZGl0b3IuUHJvamVjdC5wYXRofS9wYWNrYWdlcy9sY2MtZnJhbWV3b3JrLWNsaWVudC9iaW4vY29uZmlnYCk7XHJcblxyXG5jb25zdCBPUyA9IHJlcXVpcmUoJ29zJyk7XHJcbmNvbnN0IEZzID0gcmVxdWlyZSgnZnMnKTtcclxuY29uc3QgUGF0aCA9IHJlcXVpcmUoJ3BhdGgnKTtcclxuXHJcbmNvbnN0IGVjb25maWcgPSBnZXRFeHRlbnNpb25Db25maWcoKTtcclxuXHJcbi8qKlxyXG4gKiDmlLbpm4bmqKHlnZdcclxuICovXHJcbmZ1bmN0aW9uIGNvbGxlY3RNb2R1bGVzKCl7XHJcbiAgICBsZXQga2VlcE1vZHVsZXM6eyBba2V5OnN0cmluZ106Ym9vbGVhbiB9ID0ge307XHJcbiAgICBsZXQgbW9kdWxlRGVzY3M6eyBba2V5OnN0cmluZ106c3RyaW5nIH0gPSB7fTtcclxuICAgIGxldCBjb25maWcgPSBsb2FkQ29uZmlnKCk7XHJcblxyXG4gICAgY29uc3QgZmNvbmZpZyA9IGdldEZyYW1ld29ya0NvbmZpZygpO1xyXG4gICAgZm9yKGxldCBtbmFtZSBpbiBmY29uZmlnLm1vZHVsZXMpe1xyXG4gICAgICAgIGxldCBtb2R1bGUgPSBmY29uZmlnLm1vZHVsZXNbbW5hbWVdO1xyXG4gICAgICAgIGlmKG1uYW1lLmluZGV4T2YoXCJiYXNlXCIpICE9IDApe1xyXG4gICAgICAgICAgICBsZXQga2VlcCA9IGNvbmZpZy5wdWJsaXNoLmtlZXBNb2R1bGVzW21uYW1lXTtcclxuICAgICAgICAgICAgbW9kdWxlRGVzY3NbbW5hbWVdID0gbW9kdWxlLmRlc2NyaXB0aW9uO1xyXG4gICAgICAgICAgICBrZWVwTW9kdWxlc1ttbmFtZV0gPSBrZWVwICE9IG51bGwgPyBrZWVwIDogbW9kdWxlLmtlZXBEZWZhdWx0O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIGtlZXBzIDoga2VlcE1vZHVsZXMsXHJcbiAgICAgICAgZGVzY3MgOiBtb2R1bGVEZXNjcyxcclxuICAgIH1cclxufVxyXG5cclxuRWRpdG9yLlBhbmVsLmV4dGVuZCh7XHJcblxyXG4gICAgc3R5bGU6IEZzLnJlYWRGaWxlU3luYyhQYXRoLmpvaW4oRVhURU5TSU9OX1BBVEgsICdodG1scycsICdmb250ZWxsby5jc3MnKSwgeyBlbmNvZGluZyA6ICd1dGYtOCcgfSkgKyBcclxuXHQgICAgRnMucmVhZEZpbGVTeW5jKFBhdGguam9pbihFWFRFTlNJT05fUEFUSCwgJ2h0bWxzJywgJ3BhbmVsLWZyYW1lLmNzcycpLCB7IGVuY29kaW5nIDogJ3V0Zi04JyB9KSArIFxyXG5cdCAgICBGcy5yZWFkRmlsZVN5bmMoUGF0aC5qb2luKEVYVEVOU0lPTl9QQVRILCAnaHRtbHMnLCAnZnVuY3Rpb25zLmNzcycpLCB7IGVuY29kaW5nIDogJ3V0Zi04JyB9KSxcclxuICAgIFxyXG4gICAgdGVtcGxhdGU6IGBcclxuPGRpdiBpZD1cImZ1bmN0aW9uc1wiIGxvYWRlZD1cIlwiPlxyXG5cdDxkaXYgY2xhc3M9XCJzbGlkZXJcIj5cclxuXHRcdDxkaXYgdi1iaW5kOmFjdGl2ZT1cInNob3dwYWdlPT0wXCIgdi1vbjpjbGljaz1cImNoYW5nZVBhZ2UoMClcIj4ke0VkaXRvci5UKGVjb25maWcubmFtZSArICcubGFiZWwtYnVpbGQtcGFjay1kZWNsYXJhdGlvbicpfTwvZGl2PlxyXG5cdFx0PGRpdiB2LWJpbmQ6YWN0aXZlPVwic2hvd3BhZ2U9PTFcIiB2LW9uOmNsaWNrPVwiY2hhbmdlUGFnZSgxKVwiPiR7RWRpdG9yLlQoZWNvbmZpZy5uYW1lICsgJy5sYWJlbC1wdWJsaXNoLXNldHRpbmcnKX08L2Rpdj5cclxuXHQ8L2Rpdj5cclxuXHQ8ZGl2IGNsYXNzPVwidGFic1wiIHYtc2hvdz1cInNob3dwYWdlPT0wXCI+XHJcblx0XHQ8aGVhZGVyPlxyXG5cdFx0XHQ8aDE+JHtFZGl0b3IuVChlY29uZmlnLm5hbWUgKyAnLmxhYmVsLWJ1aWxkLXBhY2stZGVjbGFyYXRpb24nKX08L2gxPlxyXG5cdFx0PC9oZWFkZXI+XHJcblx0XHQ8c2VjdGlvbj5cclxuXHRcdFx0PGRpdiBjbGFzcz1cInNjcm9sbFwiPlxyXG5cdFx0XHRcdDxzdHlsZSB0eXBlPVwidGV4dC9jc3NcIj4uZml0LC5maXhlZC1ib3R0b20sLmZpeGVkLXRvcHtyaWdodDowfS5maXQsLmZpeGVkLWJvdHRvbSwuZml4ZWQtbGVmdHtib3R0b206MDtsZWZ0OjB9LmxheW91dC5ob3Jpem9udGFsLC5sYXlvdXQuaG9yaXpvbnRhbC1yZXZlcnNlLC5sYXlvdXQudmVydGljYWwsLmxheW91dC52ZXJ0aWNhbC1yZXZlcnNle2Rpc3BsYXk6LW1zLWZsZXhib3g7ZGlzcGxheTotd2Via2l0LWZsZXg7ZGlzcGxheTpmbGV4fS5sYXlvdXQuaW5saW5le2Rpc3BsYXk6LW1zLWlubGluZS1mbGV4Ym94O2Rpc3BsYXk6LXdlYmtpdC1pbmxpbmUtZmxleDtkaXNwbGF5OmlubGluZS1mbGV4fS5sYXlvdXQuaG9yaXpvbnRhbHstbXMtZmxleC1kaXJlY3Rpb246cm93Oy13ZWJraXQtZmxleC1kaXJlY3Rpb246cm93O2ZsZXgtZGlyZWN0aW9uOnJvd30ubGF5b3V0Lmhvcml6b250YWwtcmV2ZXJzZXstbXMtZmxleC1kaXJlY3Rpb246cm93LXJldmVyc2U7LXdlYmtpdC1mbGV4LWRpcmVjdGlvbjpyb3ctcmV2ZXJzZTtmbGV4LWRpcmVjdGlvbjpyb3ctcmV2ZXJzZX0ubGF5b3V0LnZlcnRpY2Fsey1tcy1mbGV4LWRpcmVjdGlvbjpjb2x1bW47LXdlYmtpdC1mbGV4LWRpcmVjdGlvbjpjb2x1bW47ZmxleC1kaXJlY3Rpb246Y29sdW1ufS5sYXlvdXQudmVydGljYWwtcmV2ZXJzZXstbXMtZmxleC1kaXJlY3Rpb246Y29sdW1uLXJldmVyc2U7LXdlYmtpdC1mbGV4LWRpcmVjdGlvbjpjb2x1bW4tcmV2ZXJzZTtmbGV4LWRpcmVjdGlvbjpjb2x1bW4tcmV2ZXJzZX0ubGF5b3V0LndyYXB7LW1zLWZsZXgtd3JhcDp3cmFwOy13ZWJraXQtZmxleC13cmFwOndyYXA7ZmxleC13cmFwOndyYXB9LmxheW91dC53cmFwLXJldmVyc2V7LW1zLWZsZXgtd3JhcDp3cmFwLXJldmVyc2U7LXdlYmtpdC1mbGV4LXdyYXA6d3JhcC1yZXZlcnNlO2ZsZXgtd3JhcDp3cmFwLXJldmVyc2V9LmZsZXgtYXV0b3stbXMtZmxleDoxIDEgYXV0bzstd2Via2l0LWZsZXg6MSAxIGF1dG87ZmxleDoxIDEgYXV0b30uZmxleC1ub25ley1tcy1mbGV4Om5vbmU7LXdlYmtpdC1mbGV4Om5vbmU7ZmxleDpub25lfS5mbGV4LC5mbGV4LTF7LW1zLWZsZXg6MTstd2Via2l0LWZsZXg6MTtmbGV4OjF9LmZsZXgtMnstbXMtZmxleDoyOy13ZWJraXQtZmxleDoyO2ZsZXg6Mn0uZmxleC0zey1tcy1mbGV4OjM7LXdlYmtpdC1mbGV4OjM7ZmxleDozfS5mbGV4LTR7LW1zLWZsZXg6NDstd2Via2l0LWZsZXg6NDtmbGV4OjR9LmZsZXgtNXstbXMtZmxleDo1Oy13ZWJraXQtZmxleDo1O2ZsZXg6NX0uZmxleC02ey1tcy1mbGV4OjY7LXdlYmtpdC1mbGV4OjY7ZmxleDo2fS5mbGV4LTd7LW1zLWZsZXg6Nzstd2Via2l0LWZsZXg6NztmbGV4Ojd9LmZsZXgtOHstbXMtZmxleDo4Oy13ZWJraXQtZmxleDo4O2ZsZXg6OH0uZmxleC05ey1tcy1mbGV4Ojk7LXdlYmtpdC1mbGV4Ojk7ZmxleDo5fS5mbGV4LTEwey1tcy1mbGV4OjEwOy13ZWJraXQtZmxleDoxMDtmbGV4OjEwfS5mbGV4LTExey1tcy1mbGV4OjExOy13ZWJraXQtZmxleDoxMTtmbGV4OjExfS5mbGV4LTEyey1tcy1mbGV4OjEyOy13ZWJraXQtZmxleDoxMjtmbGV4OjEyfS5sYXlvdXQuc3RhcnR7LW1zLWZsZXgtYWxpZ246c3RhcnQ7LXdlYmtpdC1hbGlnbi1pdGVtczpmbGV4LXN0YXJ0O2FsaWduLWl0ZW1zOmZsZXgtc3RhcnR9LmxheW91dC5jZW50ZXIsLmxheW91dC5jZW50ZXItY2VudGVyey1tcy1mbGV4LWFsaWduOmNlbnRlcjstd2Via2l0LWFsaWduLWl0ZW1zOmNlbnRlcjthbGlnbi1pdGVtczpjZW50ZXJ9LmxheW91dC5lbmR7LW1zLWZsZXgtYWxpZ246ZW5kOy13ZWJraXQtYWxpZ24taXRlbXM6ZmxleC1lbmQ7YWxpZ24taXRlbXM6ZmxleC1lbmR9LmxheW91dC5zdGFydC1qdXN0aWZpZWR7LW1zLWZsZXgtcGFjazpzdGFydDstd2Via2l0LWp1c3RpZnktY29udGVudDpmbGV4LXN0YXJ0O2p1c3RpZnktY29udGVudDpmbGV4LXN0YXJ0fS5sYXlvdXQuY2VudGVyLWNlbnRlciwubGF5b3V0LmNlbnRlci1qdXN0aWZpZWR7LW1zLWZsZXgtcGFjazpjZW50ZXI7LXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6Y2VudGVyO2p1c3RpZnktY29udGVudDpjZW50ZXJ9LmxheW91dC5lbmQtanVzdGlmaWVkey1tcy1mbGV4LXBhY2s6ZW5kOy13ZWJraXQtanVzdGlmeS1jb250ZW50OmZsZXgtZW5kO2p1c3RpZnktY29udGVudDpmbGV4LWVuZH0ubGF5b3V0LmFyb3VuZC1qdXN0aWZpZWR7LW1zLWZsZXgtcGFjazphcm91bmQ7LXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6c3BhY2UtYXJvdW5kO2p1c3RpZnktY29udGVudDpzcGFjZS1hcm91bmR9LmxheW91dC5qdXN0aWZpZWR7LW1zLWZsZXgtcGFjazpqdXN0aWZ5Oy13ZWJraXQtanVzdGlmeS1jb250ZW50OnNwYWNlLWJldHdlZW47anVzdGlmeS1jb250ZW50OnNwYWNlLWJldHdlZW59LnNlbGYtc3RhcnR7LW1zLWFsaWduLXNlbGY6ZmxleC1zdGFydDstd2Via2l0LWFsaWduLXNlbGY6ZmxleC1zdGFydDthbGlnbi1zZWxmOmZsZXgtc3RhcnR9LnNlbGYtY2VudGVyey1tcy1hbGlnbi1zZWxmOmNlbnRlcjstd2Via2l0LWFsaWduLXNlbGY6Y2VudGVyO2FsaWduLXNlbGY6Y2VudGVyfS5zZWxmLWVuZHstbXMtYWxpZ24tc2VsZjpmbGV4LWVuZDstd2Via2l0LWFsaWduLXNlbGY6ZmxleC1lbmQ7YWxpZ24tc2VsZjpmbGV4LWVuZH0uc2VsZi1zdHJldGNoey1tcy1hbGlnbi1zZWxmOnN0cmV0Y2g7LXdlYmtpdC1hbGlnbi1zZWxmOnN0cmV0Y2g7YWxpZ24tc2VsZjpzdHJldGNofS5ibG9ja3tkaXNwbGF5OmJsb2NrfVtoaWRkZW5de2Rpc3BsYXk6bm9uZSFpbXBvcnRhbnR9LmludmlzaWJsZXt2aXNpYmlsaXR5OmhpZGRlbiFpbXBvcnRhbnR9LnJlbGF0aXZle3Bvc2l0aW9uOnJlbGF0aXZlfS5maXR7cG9zaXRpb246YWJzb2x1dGU7dG9wOjB9Ym9keS5mdWxsYmxlZWR7bWFyZ2luOjA7aGVpZ2h0OjEwMHZofS5zY3JvbGx7LXdlYmtpdC1vdmVyZmxvdy1zY3JvbGxpbmc6dG91Y2g7b3ZlcmZsb3c6YXV0b30uZml4ZWQtYm90dG9tLC5maXhlZC1sZWZ0LC5maXhlZC1yaWdodCwuZml4ZWQtdG9we3Bvc2l0aW9uOmZpeGVkfS5maXhlZC10b3B7dG9wOjA7bGVmdDowfS5maXhlZC1yaWdodHt0b3A6MDtyaWdodDowO2JvdHRvbTowfS5maXhlZC1sZWZ0e3RvcDowfTpob3N0e2Rpc3BsYXk6YmxvY2s7cG9zaXRpb246cmVsYXRpdmU7Y29udGFpbjpjb250ZW50fXRhYmxle2JvcmRlci1jb2xsYXBzZTpjb2xsYXBzZTtmb250LXNpemU6MWVtO3dpZHRoOjEwMCV9dGQsdGh7Zm9udC13ZWlnaHQ6NDAwO3RleHQtYWxpZ246bGVmdDtwYWRkaW5nOjRweCA4cHh9LmxhYmVse3dpZHRoOjEwJTt3aGl0ZS1zcGFjZTpub3dyYXA7b3ZlcmZsb3c6aGlkZGVuO3RleHQtb3ZlcmZsb3c6ZWxsaXBzaXN9LndyYXBwZXJ7cG9zaXRpb246cmVsYXRpdmV9LmZpbGx7d2lkdGg6MTAwJX10aCB1aS1wcm9we21pbi13aWR0aDphdXRvfXRyW2hvdmVyaW5nXSAubGFiZWx7Y29sb3I6IzA5Zn10cltzZWxlY3RlZF0gLmxhYmVse2NvbG9yOiNmZDk0MmJ9dHJbaXMtZGlzYWJsZWRdIC5sYWJlbHtjb2xvcjojNjM2MzYzO2N1cnNvcjpkZWZhdWx0fSAuZGVidWcgdGQsLmRlYnVnIHRoe2JvcmRlcjoxcHggc29saWQgIzY2Nn0gLmRlYnVnIHRyOm50aC1jaGlsZChldmVuKXtiYWNrZ3JvdW5kLWNvbG9yOiMzMzN9PC9zdHlsZT5cclxuXHRcdFx0XHQ8dGFibGUgY2xhc3M9XCJkZWJ1Z1wiPlxyXG5cdFx0XHRcdFx0PHRyPlxyXG5cdFx0XHRcdFx0XHQ8dGggY2xhc3M9XCJsYWJlbFwiPlxyXG5cdFx0XHRcdFx0XHRcdDxzcGFuPiR7RWRpdG9yLlQoZWNvbmZpZy5uYW1lICsgJy5sYWJlbC1wYWNrJyl9PC9zcGFuPlxyXG5cdFx0XHRcdFx0XHQ8L3RoPlxyXG5cdFx0XHRcdFx0XHQ8dGggY2xhc3M9XCJ3cmFwcGVyXCI+XHJcblx0XHRcdFx0XHRcdFx0PHNwYW4+JHtFZGl0b3IuVChlY29uZmlnLm5hbWUgKyAnLmxhYmVsLXBhdGgnKX08L3NwYW4+XHJcblx0XHRcdFx0XHRcdDwvdGg+XHJcblx0XHRcdFx0XHRcdDx0aCBjbGFzcz1cImxhYmVsXCI+XHJcblx0XHRcdFx0XHRcdFx0PHNwYW4+JHtFZGl0b3IuVChlY29uZmlnLm5hbWUgKyAnLmxhYmVsLWFjdGlvbicpfTwvc3Bhbj5cclxuXHRcdFx0XHRcdFx0PC90aD5cclxuXHRcdFx0XHRcdDwvdHI+XHJcblx0XHRcdFx0XHQ8dHIgdi1mb3I9XCIocGFjayxwYXRoKSBpbiBwYWNrc1wiPlxyXG5cdFx0XHRcdFx0XHQ8dGggY2xhc3M9XCJsYWJlbFwiPlxyXG5cdFx0XHRcdFx0XHRcdDxzcGFuPnt7cGFja319PC9zcGFuPlxyXG5cdFx0XHRcdFx0XHQ8L3RoPlxyXG5cdFx0XHRcdFx0XHQ8dGggY2xhc3M9XCJ3cmFwcGVyXCI+XHJcblx0XHRcdFx0XHRcdFx0PHNwYW4+e3twYXRofX08L3NwYW4+XHJcblx0XHRcdFx0XHRcdDwvdGg+XHJcblx0XHRcdFx0XHRcdDx0aCBjbGFzcz1cImxhYmVsXCI+XHJcblx0XHRcdFx0XHRcdFx0PHVpLWJ1dHRvbiBjbGFzcz1cImdyZWVuXCIgdi1kaXNhYmxlZD1cImJ1aWxkaW5nXCIgQGNsaWNrPVwiYnVpbGREZWNsYXJhdGlvbihwYWNrKVwiPiR7RWRpdG9yLlQoZWNvbmZpZy5uYW1lICsgJy5idXR0b24tYnVpbGQnKX08L3VpLWJ1dHRvbj5cclxuXHRcdFx0XHRcdFx0PC90aD5cclxuXHRcdFx0XHRcdDwvdHI+XHJcblx0XHRcdFx0PC90YWJsZT5cclxuXHRcdFx0XHQ8aDI+JHtFZGl0b3IuVChlY29uZmlnLm5hbWUgKyAnLmxhYmVsLWJ1aWxkLW91dHB1dCcpfTwvaDI+XHJcblx0XHRcdFx0PHByZSB2LXRleHQ9XCJidWlsZGluZm9cIj48L3ByZT5cclxuXHRcdFx0PC9kaXY+XHJcblx0XHQ8L3NlY3Rpb24+XHJcblx0XHQ8dWktbG9hZGVyIGNsYXNzPVwidHJhbnNwYXJlbnRcIiB2LXNob3c9XCJidWlsZGluZ1wiPiR7RWRpdG9yLlQoZWNvbmZpZy5uYW1lICsgJy5sYWJlbC1idWlsZGluZycpfTwvdWktbG9hZGVyPlxyXG5cdDwvZGl2PlxyXG5cdDxkaXYgY2xhc3M9XCJ0YWJzXCIgdi1zaG93PVwic2hvd3BhZ2U9PTFcIj5cclxuXHRcdDxoZWFkZXI+XHJcblx0XHRcdDxoMT4ke0VkaXRvci5UKGVjb25maWcubmFtZSArICcubGFiZWwtcHVibGlzaC1zZXR0aW5nJyl9PC9oMT5cclxuXHRcdDwvaGVhZGVyPlxyXG5cdFx0PHNlY3Rpb24+XHJcblx0XHRcdDxkaXYgY2xhc3M9XCJwcmV2aWV3XCI+XHJcblx0XHRcdFx0PHVpLXByb3AgbmFtZT1cIiR7RWRpdG9yLlQoZWNvbmZpZy5uYW1lICsgJy5sYWJlbC1mcmFtZXdvcmstY29uZnVzZScpfVwiIHRvb2x0aXA9XCIke0VkaXRvci5UKGVjb25maWcubmFtZSArICcubGFiZWwtZnJhbWV3b3JrLWNvbmZ1c2UnKX1cIj5cclxuXHRcdFx0XHRcdDx1aS1jaGVja2JveCB2LXZhbHVlPVwicHVibGlzaC5jb25mdXNlXCI+PC91aS1jaGVja2JveD5cclxuXHRcdFx0XHQ8L3VpLXByb3A+XHJcblx0XHRcdFx0PHVpLXByb3AgbmFtZT1cIiR7RWRpdG9yLlQoZWNvbmZpZy5uYW1lICsgJy5sYWJlbC1mcmFtZXdvcmstY2xpcCcpfVwiIHRvb2x0aXA9XCIke0VkaXRvci5UKGVjb25maWcubmFtZSArICcubGFiZWwtZnJhbWV3b3JrLWNsaXAnKX1cIj5cclxuXHRcdFx0XHRcdDx1aS1jaGVja2JveCB2LXZhbHVlPVwicHVibGlzaC5jbGlwXCI+PC91aS1jaGVja2JveD5cclxuXHRcdFx0XHQ8L3VpLXByb3A+XHJcblx0XHRcdFx0PHVpLXByb3AgbmFtZT1cIiR7RWRpdG9yLlQoZWNvbmZpZy5uYW1lICsgJy5sYWJlbC1mcmFtZXdvcmstY2xpcC1tZXRob2QnKX1cIiB0b29sdGlwPVwiJHtFZGl0b3IuVChlY29uZmlnLm5hbWUgKyAnLmxhYmVsLWZyYW1ld29yay1jbGlwLW1ldGhvZCcpfVwiIHYtc2hvdz1cInB1Ymxpc2guY2xpcFwiPlxyXG5cdFx0XHRcdFx0PHVpLXNlbGVjdCB2LXZhbHVlPVwicHVibGlzaC5jbGlwTWV0aG9kXCI+XHJcblx0XHRcdFx0XHRcdDxvcHRpb24gdmFsdWU9XCJhdXRvXCI+JHtFZGl0b3IuVChlY29uZmlnLm5hbWUgKyAnLmxhYmVsLWF1dG8tY2xpcCcpfTwvb3B0aW9uPlxyXG5cdFx0XHRcdFx0XHQ8b3B0aW9uIHZhbHVlPVwiY3VzdG9tXCI+JHtFZGl0b3IuVChlY29uZmlnLm5hbWUgKyAnLmxhYmVsLWN1c3RvbS1jbGlwJyl9PC9vcHRpb24+XHJcblx0XHRcdFx0XHQ8L3VpLXNlbGVjdD5cclxuXHRcdFx0XHQ8L3VpLXByb3A+XHRcclxuXHRcdFx0XHQ8dWktc2VjdGlvbiB2LXNob3c9XCJwdWJsaXNoLmNsaXAmJnB1Ymxpc2guY2xpcE1ldGhvZD09J2N1c3RvbSdcIj5cclxuXHRcdFx0XHRcdDxkaXYgc2xvdD1cImhlYWRlclwiPiR7RWRpdG9yLlQoZWNvbmZpZy5uYW1lICsgJy5sYWJlbC1rZWVwLW1vZHVsZXMnKX08L2Rpdj5cclxuXHRcdFx0XHRcdDxkaXYgc3R5bGU9XCJvdmVyZmxvdzogYXV0bztoZWlnaHQ6IDIwMHB4O1wiPlxyXG5cdFx0XHRcdFx0XHQ8dWktcHJvcCB2LWZvcj1cIihtb2R1bGUsa2VlcCkgaW4gcHVibGlzaC5rZWVwTW9kdWxlc1wiIDprZXk9XCJtb2R1bGVcIiA6bmFtZT1cIm1vZHVsZVwiIDp0b29sdGlwPVwibW9kdWxlRGVzY3NbbW9kdWxlXVwiPlxyXG5cdFx0XHRcdFx0XHRcdDx1aS1jaGVja2JveCB2LXZhbHVlPVwia2VlcFwiPjwvdWktY2hlY2tib3g+XHJcblx0XHRcdFx0XHRcdDwvdWktcHJvcD5cclxuXHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdDwvdWktc2VjdGlvbj5cclxuXHRcdFx0PC9kaXY+XHJcblx0XHQ8L3NlY3Rpb24+XHJcblx0XHQ8Zm9vdGVyPlxyXG4gICAgICAgICAgICA8dWktYnV0dG9uIGNsYXNzPVwiZ3JlZW5cIiB2LWRpc2FibGVkPVwic2F2aW5nXCIgQGNsaWNrPVwic2F2ZVB1Ymxpc2hDb25maWcoKVwiPiR7RWRpdG9yLlQoZWNvbmZpZy5uYW1lICsgJy5idXR0b24tc2F2ZScpfTwvdWktYnV0dG9uPlxyXG4gICAgICAgIDwvZm9vdGVyPlxyXG5cdFx0PHVpLWxvYWRlciBjbGFzcz1cInRyYW5zcGFyZW50XCIgdi1zaG93PVwic2F2aW5nXCI+JHtFZGl0b3IuVChlY29uZmlnLm5hbWUgKyAnLmxhYmVsLXNhdmluZycpfTwvdWktbG9hZGVyPlxyXG5cdDwvZGl2PlxyXG48L2Rpdj5cclxuYCxcclxuICBcclxuICAgIGFzeW5jIHJlYWR5KCkge1xyXG4gICAgICAgIGxldCBjb25maWcgPSBsb2FkQ29uZmlnKCk7XHJcbiAgICAgICAgbGV0IG1vZHVsZXMgPSBjb2xsZWN0TW9kdWxlcygpO1xyXG5cclxuICAgICAgICAvLyBAdHMtaWdub3JlXHJcbiAgICAgICAgY29uc3QgYXBwID0gbmV3IHdpbmRvdy5WdWUoe1xyXG4gICAgICAgICAgICBlbDogdGhpcy5zaGFkb3dSb290LFxyXG5cclxuICAgICAgICAgICAgZGF0YSgpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2hvd3BhZ2UgOiAwLFxyXG4gICAgICAgICAgICAgICAgICAgIHBhY2tzIDogZ2V0R2FtZVBhY2tzKCksXHJcbiAgICAgICAgICAgICAgICAgICAgYnVpbGRpbmcgOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICBidWlsZGluZm8gOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICBtb2R1bGVEZXNjcyA6IG1vZHVsZXMuZGVzY3MsXHJcbiAgICAgICAgICAgICAgICAgICAgcHVibGlzaCA6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uZnVzZSA6IGNvbmZpZy5wdWJsaXNoLmNvbmZ1c2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsaXAgOiBjb25maWcucHVibGlzaC5jbGlwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGlwTWV0aG9kIDogY29uZmlnLnB1Ymxpc2guY2xpcE1ldGhvZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAga2VlcE1vZHVsZXMgOiBtb2R1bGVzLmtlZXBzLFxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgc2F2aW5nIDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBtZXRob2RzOiB7XHJcbiAgICAgICAgICAgICAgICBjaGFuZ2VQYWdlKHBhZ2U6bnVtYmVyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYodGhpcy5zaG93cGFnZSAhPSBwYWdlKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zaG93cGFnZSA9IHBhZ2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKHBhZ2UgPT0gMCl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnBhY2tzID0gZ2V0R2FtZVBhY2tzKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1lbHNlIGlmKHBhZ2UgPT0gMSl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtb2R1bGVzID0gY29sbGVjdE1vZHVsZXMoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubW9kdWxlRGVzY3MgPSBtb2R1bGVzLmRlc2NzO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5wdWJsaXNoLmtlZXBNb2R1bGVzID0gbW9kdWxlcy5rZWVwcztcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIGFzeW5jIGJ1aWxkRGVjbGFyYXRpb24ocGFjazpzdHJpbmcpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmJ1aWxkaW5nID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmJ1aWxkaW5mbyA9IGAke0VkaXRvci5UKGVjb25maWcubmFtZSArICcubGFiZWwtcGFjay1idWlsZGluZycpfSBbJHtwYWNrfV1gO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKE9TLnBsYXRmb3JtKCkgPT0gXCJ3aW4zMlwiKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGNvbW1hbmQgPSBgbnB4IGd1bHAgYnVpbGREZWNsYXJhdGlvbiAtLXByb2plY3QgJHtFZGl0b3IuUHJvamVjdC5wYXRofSAtLXBhY2sgJHtwYWNrfWA7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCByZXN1bHQgPSBhd2FpdCBleGVDb21tYW5kKGNvbW1hbmQsIEVYVEVOU0lPTl9QQVRIKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3VsdC5lcnJvcikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5idWlsZGluZm8gKz0gYFxcbmVycm9yIDpcXG4ke3Jlc3VsdC5lcnJvcn1gO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYnVpbGRpbmZvICs9IGBcXG5zdGRvdXQgOlxcbiR7cmVzdWx0LnN0ZG91dH1gO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYocmVzdWx0LnN0ZGVycil7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5idWlsZGluZm8gKz0gYFxcbnN0ZGVyciA6XFxuJHtyZXN1bHQuc3RkZXJyfWA7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5idWlsZGluZm8gKz0gYFxcbiR7RWRpdG9yLlQoZWNvbmZpZy5uYW1lICsgJy5sYWJlbC1wYWNrLWJ1aWxkZWQnKX1gO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYnVpbGRpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIGFzeW5jIHNhdmVQdWJsaXNoQ29uZmlnKCl7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zYXZpbmcgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbmZpZy5wdWJsaXNoID0gdGhpcy5wdWJsaXNoO1xyXG4gICAgICAgICAgICAgICAgICAgIHNhdmVDb25maWcoY29uZmlnKTsgXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zYXZpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59KTtcclxuIl19
