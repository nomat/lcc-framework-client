"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getGamePacks = exports.getExtensionConfig = exports.getFrameworkConfig = exports.copyDirectory = exports.copyFile = exports.mapDirectory = exports.findFile = exports.FindFileType = exports.updateMap = exports.exeCommand = exports.EXTENSION_PATH = void 0;
const Fs = require('fs');
const Path = require('path');
const { exec } = require('child_process');
/**
 * 扩展路径
 */
exports.EXTENSION_PATH = Path.join(__dirname, '..');
/**
 * 执行命令
 * @param command
 * @param cwd
 */
function exeCommand(command, cwd) {
    return new Promise((resolve) => {
        return exec(command, { cwd: cwd }, (error, stdout, stderr) => {
            resolve({
                error: error,
                stdout: stdout,
                stderr: stderr,
            });
        });
    });
}
exports.exeCommand = exeCommand;
/**
 * @zh
 * 更新Map表<br/>
 * Map表更新规则：<br/>
 * 遍历源Map表字段，对比目的Map表对应字段，存在几种情况：<br/>
 * 1、目的Map表中不存在该字段，直接设置为源Map表字段<br/>
 * 2、目的Map表字段和源Map表字段同为数组(Array)，直接设置为源Map表字段<br/>
 * 3、目的Map表字段和源Map表字段同为对象(Object)，以字段的目的Map表和源Map表为参数再次调用次函数<br/>
 *
 * @param dmap - 目的Map表
 * @param smap - 源Map表
 */
function updateMap(dmap, smap) {
    for (let key in smap) {
        let svalue = smap[key];
        let dvalue = dmap[key];
        if (dvalue == null) {
            dmap[key] = svalue;
        }
        else {
            let stype = typeof svalue;
            let dtype = typeof dvalue;
            if (dtype == 'object' && stype == dtype) {
                let sarray = svalue instanceof Array;
                let darray = dvalue instanceof Array;
                if (sarray != darray) {
                    dmap[key] = svalue;
                }
                else {
                    if (darray) {
                        dmap[key] = svalue;
                    }
                    else {
                        this.updateMap(dvalue, svalue);
                    }
                }
            }
            else {
                dmap[key] = svalue;
            }
        }
    }
}
exports.updateMap = updateMap;
/**
 * 查找文件类型
 */
var FindFileType;
(function (FindFileType) {
    FindFileType[FindFileType["FILE"] = 1] = "FILE";
    FindFileType[FindFileType["DIRECTORY"] = 2] = "DIRECTORY";
})(FindFileType = exports.FindFileType || (exports.FindFileType = {}));
/**
 * 在指定目录中查找文件
 * @param dir
 * @param filename
 */
function findFile(dir, filename, types = FindFileType.FILE | FindFileType.DIRECTORY) {
    for (let file of Fs.readdirSync(dir)) {
        let npath = Path.join(dir, file);
        var info = Fs.statSync(npath);
        if ((file == filename) && (info.isDirectory() ? types & FindFileType.DIRECTORY : types & FindFileType.FILE)) {
            return npath;
        }
        else if (info.isDirectory()) {
            let result = findFile(npath, filename, types);
            if (result) {
                return result;
            }
        }
    }
}
exports.findFile = findFile;
/**
 * 遍历目录路径
 */
function mapDirectory(path_, callback) {
    for (let file of Fs.readdirSync(path_)) {
        let npath = Path.join(path_, file);
        var info = Fs.statSync(npath);
        if (callback(npath, file, info.isDirectory())) {
            return true;
        }
        else if (info.isDirectory()) {
            if (mapDirectory(npath, callback)) {
                return true;
            }
        }
    }
}
exports.mapDirectory = mapDirectory;
/**
 * 复制文件
 * @param srcPath
 * @param destPath
 */
function copyFile(srcPath, destPath) {
    Fs.writeFileSync(destPath, Fs.readFileSync(srcPath));
}
exports.copyFile = copyFile;
/**
 * 复制目录
 * @param src
 * @param dest
 */
function copyDirectory(srcPath, destPath) {
    if (Fs.existsSync(srcPath)) {
        if (!Fs.existsSync(destPath)) {
            Fs.mkdirSync(destPath, { recursive: true });
        }
        for (let file of Fs.readdirSync(srcPath)) {
            let spath = Path.join(srcPath, file);
            let dpath = Path.join(destPath, file);
            var info = Fs.statSync(spath);
            if (info.isDirectory()) {
                copyDirectory(spath, dpath);
            }
            else {
                copyFile(spath, dpath);
            }
        }
    }
}
exports.copyDirectory = copyDirectory;
/**
 * 获得框架配置
 */
const modulesFile = Path.join(exports.EXTENSION_PATH, 'framework', 'config.json');
let frameworkConfig = null;
function getFrameworkConfig() {
    if (!frameworkConfig) {
        if (Fs.existsSync(modulesFile)) {
            frameworkConfig = JSON.parse(Fs.readFileSync(modulesFile, { encoding: 'utf-8' }));
        }
    }
    return frameworkConfig;
}
exports.getFrameworkConfig = getFrameworkConfig;
/**
 * 获得扩展配置
 */
const packageFile = Path.join(exports.EXTENSION_PATH, 'package.json');
let extensionConfig = null;
function getExtensionConfig() {
    if (!extensionConfig) {
        if (Fs.existsSync(packageFile)) {
            extensionConfig = JSON.parse(Fs.readFileSync(packageFile, { encoding: 'utf-8' }));
        }
    }
    return extensionConfig;
}
exports.getExtensionConfig = getExtensionConfig;
/**
 * 获得游戏包
 */
function getGamePacks() {
    let packs = {};
    let _collectGamePacks;
    _collectGamePacks = (path) => {
        for (let f of Fs.readdirSync(path)) {
            let fpath = Path.join(path, f);
            let fstate = Fs.statSync(fpath);
            if (fstate.isDirectory()) {
                let metafile = fpath + ".meta";
                if (Fs.existsSync(metafile)) {
                    let meta = JSON.parse(Fs.readFileSync(metafile, { encoding: 'utf-8' }));
                    if (meta && meta.isBundle) {
                        packs[meta.bundleName || f] = Path.normalize(fpath);
                        continue;
                    }
                }
                _collectGamePacks(fpath);
            }
        }
    };
    packs['main'] = Path.normalize(Path.join(Editor.Project.path, 'assets'));
    _collectGamePacks(Path.join(Editor.Project.path, 'assets'));
    return packs;
}
exports.getGamePacks = getGamePacks;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInV0aWxzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUVBLE1BQU0sRUFBRSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUN6QixNQUFNLElBQUksR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7QUFDN0IsTUFBTSxFQUFFLElBQUksRUFBRSxHQUFHLE9BQU8sQ0FBQyxlQUFlLENBQUMsQ0FBQztBQUUxQzs7R0FFRztBQUNVLFFBQUEsY0FBYyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO0FBRXpEOzs7O0dBSUc7QUFDSCxTQUFnQixVQUFVLENBQUMsT0FBYyxFQUFFLEdBQVU7SUFDcEQsT0FBTyxJQUFJLE9BQU8sQ0FBb0QsQ0FBQyxPQUFPLEVBQUMsRUFBRTtRQUNoRixPQUFPLElBQUksQ0FBQyxPQUFPLEVBQUUsRUFBRSxHQUFHLEVBQUcsR0FBRyxFQUFFLEVBQUUsQ0FBQyxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBQzdELE9BQU8sQ0FBQztnQkFDUCxLQUFLLEVBQUcsS0FBSztnQkFDYixNQUFNLEVBQUcsTUFBTTtnQkFDZixNQUFNLEVBQUcsTUFBTTthQUNmLENBQUMsQ0FBQztRQUNKLENBQUMsQ0FBQyxDQUFDO0lBQ0osQ0FBQyxDQUFDLENBQUM7QUFDSixDQUFDO0FBVkQsZ0NBVUM7QUFFRDs7Ozs7Ozs7Ozs7R0FXRztBQUNGLFNBQWdCLFNBQVMsQ0FBQyxJQUFXLEVBQUUsSUFBVztJQUMvQyxLQUFJLElBQUksR0FBRyxJQUFJLElBQUksRUFBQztRQUNoQixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdkIsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3ZCLElBQUcsTUFBTSxJQUFJLElBQUksRUFBQztZQUNkLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxNQUFNLENBQUM7U0FDdEI7YUFBSTtZQUNELElBQUksS0FBSyxHQUFHLE9BQU8sTUFBTSxDQUFDO1lBQzFCLElBQUksS0FBSyxHQUFHLE9BQU8sTUFBTSxDQUFDO1lBQzFCLElBQUcsS0FBSyxJQUFJLFFBQVEsSUFBSSxLQUFLLElBQUksS0FBSyxFQUFDO2dCQUNuQyxJQUFJLE1BQU0sR0FBRyxNQUFNLFlBQVksS0FBSyxDQUFDO2dCQUNyQyxJQUFJLE1BQU0sR0FBRyxNQUFNLFlBQVksS0FBSyxDQUFDO2dCQUNyQyxJQUFHLE1BQU0sSUFBSSxNQUFNLEVBQUM7b0JBQ2hCLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxNQUFNLENBQUM7aUJBQ3RCO3FCQUFJO29CQUNELElBQUcsTUFBTSxFQUFDO3dCQUNOLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxNQUFNLENBQUM7cUJBQ3RCO3lCQUFLO3dCQUNGLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO3FCQUNsQztpQkFDSjthQUNKO2lCQUFJO2dCQUNELElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxNQUFNLENBQUM7YUFDdEI7U0FDSjtLQUNKO0FBQ0wsQ0FBQztBQTFCQSw4QkEwQkE7QUFFRDs7R0FFRztBQUNILElBQVksWUFHWDtBQUhELFdBQVksWUFBWTtJQUN2QiwrQ0FBUSxDQUFBO0lBQ1IseURBQWEsQ0FBQTtBQUNkLENBQUMsRUFIVyxZQUFZLEdBQVosb0JBQVksS0FBWixvQkFBWSxRQUd2QjtBQUVEOzs7O0dBSUc7QUFDSCxTQUFnQixRQUFRLENBQUMsR0FBVSxFQUFFLFFBQWUsRUFBRSxRQUFlLFlBQVksQ0FBQyxJQUFJLEdBQUcsWUFBWSxDQUFDLFNBQVM7SUFDOUcsS0FBSSxJQUFJLElBQUksSUFBSSxFQUFFLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFDO1FBQ25DLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ2pDLElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDOUIsSUFBRyxDQUFDLElBQUksSUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxZQUFZLENBQUMsSUFBSSxDQUFDLEVBQUM7WUFDMUcsT0FBTyxLQUFLLENBQUM7U0FDYjthQUFLLElBQUcsSUFBSSxDQUFDLFdBQVcsRUFBRSxFQUFDO1lBQzNCLElBQUksTUFBTSxHQUFHLFFBQVEsQ0FBQyxLQUFLLEVBQUUsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQzlDLElBQUcsTUFBTSxFQUFDO2dCQUNULE9BQU8sTUFBTSxDQUFDO2FBQ2Q7U0FDRDtLQUNEO0FBQ0YsQ0FBQztBQWJELDRCQWFDO0FBRUQ7O0dBRUc7QUFDSCxTQUFnQixZQUFZLENBQUMsS0FBWSxFQUFFLFFBQTJEO0lBQ3JHLEtBQUksSUFBSSxJQUFJLElBQUksRUFBRSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBQztRQUNyQyxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNuQyxJQUFJLElBQUksR0FBRyxFQUFFLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzlCLElBQUcsUUFBUSxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLEVBQUM7WUFDNUMsT0FBTyxJQUFJLENBQUM7U0FDWjthQUFLLElBQUcsSUFBSSxDQUFDLFdBQVcsRUFBRSxFQUFDO1lBQzNCLElBQUcsWUFBWSxDQUFDLEtBQUssRUFBRSxRQUFRLENBQUMsRUFBQztnQkFDaEMsT0FBTyxJQUFJLENBQUM7YUFDWjtTQUNEO0tBQ0Q7QUFDRixDQUFDO0FBWkQsb0NBWUM7QUFFRDs7OztHQUlHO0FBQ0gsU0FBZ0IsUUFBUSxDQUFDLE9BQWMsRUFBRSxRQUFlO0lBQ3ZELEVBQUUsQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFFLEVBQUUsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztBQUN0RCxDQUFDO0FBRkQsNEJBRUM7QUFFRDs7OztHQUlHO0FBQ0gsU0FBZ0IsYUFBYSxDQUFDLE9BQWMsRUFBRSxRQUFlO0lBQ3pELElBQUcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsRUFBQztRQUN0QixJQUFHLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBQztZQUN4QixFQUFFLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxFQUFFLFNBQVMsRUFBRyxJQUFJLEVBQUUsQ0FBQyxDQUFDO1NBQ2hEO1FBQ0QsS0FBSSxJQUFJLElBQUksSUFBSSxFQUFFLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxFQUFDO1lBQ3BDLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ3JDLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ3RDLElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDOUIsSUFBRyxJQUFJLENBQUMsV0FBVyxFQUFFLEVBQUM7Z0JBQ2xCLGFBQWEsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUM7YUFDL0I7aUJBQUk7Z0JBQ0QsUUFBUSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQzthQUMxQjtTQUNKO0tBQ0o7QUFDTCxDQUFDO0FBaEJELHNDQWdCQztBQUVEOztHQUVHO0FBQ0gsTUFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxzQkFBYyxFQUFFLFdBQVcsRUFBRSxhQUFhLENBQUMsQ0FBQztBQUMxRSxJQUFJLGVBQWUsR0FBbUIsSUFBSSxDQUFDO0FBQzNDLFNBQWdCLGtCQUFrQjtJQUM5QixJQUFHLENBQUMsZUFBZSxFQUFDO1FBQ2hCLElBQUcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsRUFBQztZQUMxQixlQUFlLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxFQUFFLFFBQVEsRUFBRyxPQUFPLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDdEY7S0FDSjtJQUNELE9BQU8sZUFBZSxDQUFDO0FBQzNCLENBQUM7QUFQRCxnREFPQztBQUVEOztHQUVHO0FBQ0gsTUFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxzQkFBYyxFQUFFLGNBQWMsQ0FBQyxDQUFDO0FBQzlELElBQUksZUFBZSxHQUFHLElBQUksQ0FBQztBQUMzQixTQUFnQixrQkFBa0I7SUFDOUIsSUFBRyxDQUFDLGVBQWUsRUFBQztRQUNoQixJQUFHLEVBQUUsQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLEVBQUM7WUFDMUIsZUFBZSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsRUFBRSxRQUFRLEVBQUcsT0FBTyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ3RGO0tBQ0o7SUFDRCxPQUFPLGVBQWUsQ0FBQztBQUMzQixDQUFDO0FBUEQsZ0RBT0M7QUFFRDs7R0FFRztBQUNILFNBQWdCLFlBQVk7SUFDeEIsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDO0lBQ2YsSUFBSSxpQkFBaUIsQ0FBQztJQUN0QixpQkFBaUIsR0FBRyxDQUFDLElBQVcsRUFBQyxFQUFFO1FBQy9CLEtBQUksSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBQztZQUM5QixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztZQUMvQixJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2hDLElBQUcsTUFBTSxDQUFDLFdBQVcsRUFBRSxFQUFDO2dCQUNwQixJQUFJLFFBQVEsR0FBRyxLQUFLLEdBQUcsT0FBTyxDQUFDO2dCQUMvQixJQUFHLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUM7b0JBQ3ZCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsRUFBRSxRQUFRLEVBQUcsT0FBTyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUN6RSxJQUFHLElBQUksSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFDO3dCQUNyQixLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUNwRCxTQUFTO3FCQUNaO2lCQUNKO2dCQUNELGlCQUFpQixDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQzVCO1NBQ0o7SUFDTCxDQUFDLENBQUM7SUFDTCxLQUFLLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQyxDQUFDLENBQUM7SUFDekUsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQyxDQUFDO0lBQ3pELE9BQU8sS0FBSyxDQUFDO0FBQ2pCLENBQUM7QUF2QkQsb0NBdUJDIiwiZmlsZSI6InV0aWxzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRnJhbWV3b3JrQ29uZmlnIH0gZnJvbSBcIi4vYnVpbGRlci9mcmFtZXdvcmstY29uZmlnXCI7XHJcblxyXG5jb25zdCBGcyA9IHJlcXVpcmUoJ2ZzJyk7XHJcbmNvbnN0IFBhdGggPSByZXF1aXJlKCdwYXRoJyk7XHJcbmNvbnN0IHsgZXhlYyB9ID0gcmVxdWlyZSgnY2hpbGRfcHJvY2VzcycpO1xyXG5cclxuLyoqXHJcbiAqIOaJqeWxlei3r+W+hFxyXG4gKi9cclxuZXhwb3J0IGNvbnN0IEVYVEVOU0lPTl9QQVRIID0gUGF0aC5qb2luKF9fZGlybmFtZSwgJy4uJyk7XHJcblxyXG4vKipcclxuICog5omn6KGM5ZG95LukXHJcbiAqIEBwYXJhbSBjb21tYW5kIFxyXG4gKiBAcGFyYW0gY3dkIFxyXG4gKi9cclxuZXhwb3J0IGZ1bmN0aW9uIGV4ZUNvbW1hbmQoY29tbWFuZDpzdHJpbmcsIGN3ZDpzdHJpbmcpe1xyXG5cdHJldHVybiBuZXcgUHJvbWlzZTx7IGVycm9yPzpzdHJpbmcsIHN0ZG91dD86c3RyaW5nLCBzdGRlcnI/OnN0cmluZyB9PigocmVzb2x2ZSk9PntcclxuXHRcdHJldHVybiBleGVjKGNvbW1hbmQsIHsgY3dkIDogY3dkIH0sIChlcnJvciwgc3Rkb3V0LCBzdGRlcnIpID0+IHtcclxuXHRcdFx0cmVzb2x2ZSh7XHJcblx0XHRcdFx0ZXJyb3IgOiBlcnJvcixcclxuXHRcdFx0XHRzdGRvdXQgOiBzdGRvdXQsXHJcblx0XHRcdFx0c3RkZXJyIDogc3RkZXJyLFxyXG5cdFx0XHR9KTtcclxuXHRcdH0pO1xyXG5cdH0pO1xyXG59XHJcblxyXG4vKipcclxuICogQHpoXHJcbiAqIOabtOaWsE1hcOihqDxici8+XHJcbiAqIE1hcOihqOabtOaWsOinhOWIme+8mjxici8+XHJcbiAqIOmBjeWOhua6kE1hcOihqOWtl+aute+8jOWvueavlOebrueahE1hcOihqOWvueW6lOWtl+aute+8jOWtmOWcqOWHoOenjeaDheWGte+8mjxici8+XHJcbiAqIDHjgIHnm67nmoRNYXDooajkuK3kuI3lrZjlnKjor6XlrZfmrrXvvIznm7TmjqXorr7nva7kuLrmupBNYXDooajlrZfmrrU8YnIvPlxyXG4gKiAy44CB55uu55qETWFw6KGo5a2X5q615ZKM5rqQTWFw6KGo5a2X5q615ZCM5Li65pWw57uEKEFycmF5Ke+8jOebtOaOpeiuvue9ruS4uua6kE1hcOihqOWtl+autTxici8+XHJcbiAqIDPjgIHnm67nmoRNYXDooajlrZfmrrXlkozmupBNYXDooajlrZfmrrXlkIzkuLrlr7nosaEoT2JqZWN0Ke+8jOS7peWtl+auteeahOebrueahE1hcOihqOWSjOa6kE1hcOihqOS4uuWPguaVsOWGjeasoeiwg+eUqOasoeWHveaVsDxici8+XHJcbiAqIFxyXG4gKiBAcGFyYW0gZG1hcCAtIOebrueahE1hcOihqFxyXG4gKiBAcGFyYW0gc21hcCAtIOa6kE1hcOihqFxyXG4gKi9cclxuIGV4cG9ydCBmdW5jdGlvbiB1cGRhdGVNYXAoZG1hcDpPYmplY3QsIHNtYXA6T2JqZWN0KXtcclxuICAgIGZvcihsZXQga2V5IGluIHNtYXApe1xyXG4gICAgICAgIGxldCBzdmFsdWUgPSBzbWFwW2tleV07XHJcbiAgICAgICAgbGV0IGR2YWx1ZSA9IGRtYXBba2V5XTtcclxuICAgICAgICBpZihkdmFsdWUgPT0gbnVsbCl7XHJcbiAgICAgICAgICAgIGRtYXBba2V5XSA9IHN2YWx1ZTtcclxuICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgbGV0IHN0eXBlID0gdHlwZW9mIHN2YWx1ZTtcclxuICAgICAgICAgICAgbGV0IGR0eXBlID0gdHlwZW9mIGR2YWx1ZTtcclxuICAgICAgICAgICAgaWYoZHR5cGUgPT0gJ29iamVjdCcgJiYgc3R5cGUgPT0gZHR5cGUpe1xyXG4gICAgICAgICAgICAgICAgbGV0IHNhcnJheSA9IHN2YWx1ZSBpbnN0YW5jZW9mIEFycmF5O1xyXG4gICAgICAgICAgICAgICAgbGV0IGRhcnJheSA9IGR2YWx1ZSBpbnN0YW5jZW9mIEFycmF5O1xyXG4gICAgICAgICAgICAgICAgaWYoc2FycmF5ICE9IGRhcnJheSl7XHJcbiAgICAgICAgICAgICAgICAgICAgZG1hcFtrZXldID0gc3ZhbHVlO1xyXG4gICAgICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYoZGFycmF5KXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZG1hcFtrZXldID0gc3ZhbHVlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1lbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51cGRhdGVNYXAoZHZhbHVlLCBzdmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICBkbWFwW2tleV0gPSBzdmFsdWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiDmn6Xmib7mlofku7bnsbvlnotcclxuICovXHJcbmV4cG9ydCBlbnVtIEZpbmRGaWxlVHlwZSB7XHJcblx0RklMRSA9IDEsXHJcblx0RElSRUNUT1JZID0gMixcclxufVxyXG5cclxuLyoqXHJcbiAqIOWcqOaMh+WumuebruW9leS4reafpeaJvuaWh+S7tlxyXG4gKiBAcGFyYW0gZGlyIFxyXG4gKiBAcGFyYW0gZmlsZW5hbWUgXHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gZmluZEZpbGUoZGlyOnN0cmluZywgZmlsZW5hbWU6c3RyaW5nLCB0eXBlczpudW1iZXIgPSBGaW5kRmlsZVR5cGUuRklMRSB8IEZpbmRGaWxlVHlwZS5ESVJFQ1RPUlkpe1xyXG5cdGZvcihsZXQgZmlsZSBvZiBGcy5yZWFkZGlyU3luYyhkaXIpKXtcclxuXHRcdGxldCBucGF0aCA9IFBhdGguam9pbihkaXIsIGZpbGUpO1xyXG5cdFx0dmFyIGluZm8gPSBGcy5zdGF0U3luYyhucGF0aCk7XHJcblx0XHRpZigoZmlsZSA9PSBmaWxlbmFtZSkgJiYgKGluZm8uaXNEaXJlY3RvcnkoKSA/IHR5cGVzICYgRmluZEZpbGVUeXBlLkRJUkVDVE9SWSA6IHR5cGVzICYgRmluZEZpbGVUeXBlLkZJTEUpKXtcclxuXHRcdFx0cmV0dXJuIG5wYXRoO1xyXG5cdFx0fWVsc2UgaWYoaW5mby5pc0RpcmVjdG9yeSgpKXtcclxuXHRcdFx0bGV0IHJlc3VsdCA9IGZpbmRGaWxlKG5wYXRoLCBmaWxlbmFtZSwgdHlwZXMpO1xyXG5cdFx0XHRpZihyZXN1bHQpe1xyXG5cdFx0XHRcdHJldHVybiByZXN1bHQ7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiDpgY3ljobnm67lvZXot6/lvoRcclxuICovXHJcbmV4cG9ydCBmdW5jdGlvbiBtYXBEaXJlY3RvcnkocGF0aF86c3RyaW5nLCBjYWxsYmFjazooUGF0aDpzdHJpbmcsIGZpbGU6c3RyaW5nLCBpc2Rpcjpib29sZWFuKT0+Ym9vbGVhbikge1xyXG5cdGZvcihsZXQgZmlsZSBvZiBGcy5yZWFkZGlyU3luYyhwYXRoXykpe1xyXG5cdFx0bGV0IG5wYXRoID0gUGF0aC5qb2luKHBhdGhfLCBmaWxlKTtcclxuXHRcdHZhciBpbmZvID0gRnMuc3RhdFN5bmMobnBhdGgpO1xyXG5cdFx0aWYoY2FsbGJhY2sobnBhdGgsIGZpbGUsIGluZm8uaXNEaXJlY3RvcnkoKSkpe1xyXG5cdFx0XHRyZXR1cm4gdHJ1ZTtcclxuXHRcdH1lbHNlIGlmKGluZm8uaXNEaXJlY3RvcnkoKSl7XHJcblx0XHRcdGlmKG1hcERpcmVjdG9yeShucGF0aCwgY2FsbGJhY2spKXtcclxuXHRcdFx0XHRyZXR1cm4gdHJ1ZTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuLyoqXHJcbiAqIOWkjeWItuaWh+S7tlxyXG4gKiBAcGFyYW0gc3JjUGF0aCBcclxuICogQHBhcmFtIGRlc3RQYXRoIFxyXG4gKi9cclxuZXhwb3J0IGZ1bmN0aW9uIGNvcHlGaWxlKHNyY1BhdGg6c3RyaW5nLCBkZXN0UGF0aDpzdHJpbmcpe1xyXG5cdEZzLndyaXRlRmlsZVN5bmMoZGVzdFBhdGgsIEZzLnJlYWRGaWxlU3luYyhzcmNQYXRoKSk7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiDlpI3liLbnm67lvZVcclxuICogQHBhcmFtIHNyYyBcclxuICogQHBhcmFtIGRlc3QgXHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gY29weURpcmVjdG9yeShzcmNQYXRoOnN0cmluZywgZGVzdFBhdGg6c3RyaW5nKXtcclxuICAgIGlmKEZzLmV4aXN0c1N5bmMoc3JjUGF0aCkpe1xyXG4gICAgICAgIGlmKCFGcy5leGlzdHNTeW5jKGRlc3RQYXRoKSl7XHJcbiAgICAgICAgICAgIEZzLm1rZGlyU3luYyhkZXN0UGF0aCwgeyByZWN1cnNpdmUgOiB0cnVlIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBmb3IobGV0IGZpbGUgb2YgRnMucmVhZGRpclN5bmMoc3JjUGF0aCkpe1xyXG4gICAgICAgICAgICBsZXQgc3BhdGggPSBQYXRoLmpvaW4oc3JjUGF0aCwgZmlsZSk7XHJcbiAgICAgICAgICAgIGxldCBkcGF0aCA9IFBhdGguam9pbihkZXN0UGF0aCwgZmlsZSk7XHJcbiAgICAgICAgICAgIHZhciBpbmZvID0gRnMuc3RhdFN5bmMoc3BhdGgpO1xyXG4gICAgICAgICAgICBpZihpbmZvLmlzRGlyZWN0b3J5KCkpe1xyXG4gICAgICAgICAgICAgICAgY29weURpcmVjdG9yeShzcGF0aCwgZHBhdGgpO1xyXG4gICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgIGNvcHlGaWxlKHNwYXRoLCBkcGF0aCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiDojrflvpfmoYbmnrbphY3nva5cclxuICovXHJcbmNvbnN0IG1vZHVsZXNGaWxlID0gUGF0aC5qb2luKEVYVEVOU0lPTl9QQVRILCAnZnJhbWV3b3JrJywgJ2NvbmZpZy5qc29uJyk7XHJcbmxldCBmcmFtZXdvcmtDb25maWc6RnJhbWV3b3JrQ29uZmlnID0gbnVsbDtcclxuZXhwb3J0IGZ1bmN0aW9uIGdldEZyYW1ld29ya0NvbmZpZygpe1xyXG4gICAgaWYoIWZyYW1ld29ya0NvbmZpZyl7XHJcbiAgICAgICAgaWYoRnMuZXhpc3RzU3luYyhtb2R1bGVzRmlsZSkpe1xyXG4gICAgICAgICAgICBmcmFtZXdvcmtDb25maWcgPSBKU09OLnBhcnNlKEZzLnJlYWRGaWxlU3luYyhtb2R1bGVzRmlsZSwgeyBlbmNvZGluZyA6ICd1dGYtOCcgfSkpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIHJldHVybiBmcmFtZXdvcmtDb25maWc7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiDojrflvpfmianlsZXphY3nva5cclxuICovXHJcbmNvbnN0IHBhY2thZ2VGaWxlID0gUGF0aC5qb2luKEVYVEVOU0lPTl9QQVRILCAncGFja2FnZS5qc29uJyk7XHJcbmxldCBleHRlbnNpb25Db25maWcgPSBudWxsO1xyXG5leHBvcnQgZnVuY3Rpb24gZ2V0RXh0ZW5zaW9uQ29uZmlnKCl7XHJcbiAgICBpZighZXh0ZW5zaW9uQ29uZmlnKXtcclxuICAgICAgICBpZihGcy5leGlzdHNTeW5jKHBhY2thZ2VGaWxlKSl7XHJcbiAgICAgICAgICAgIGV4dGVuc2lvbkNvbmZpZyA9IEpTT04ucGFyc2UoRnMucmVhZEZpbGVTeW5jKHBhY2thZ2VGaWxlLCB7IGVuY29kaW5nIDogJ3V0Zi04JyB9KSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgcmV0dXJuIGV4dGVuc2lvbkNvbmZpZztcclxufVxyXG5cclxuLyoqXHJcbiAqIOiOt+W+l+a4uOaIj+WMhVxyXG4gKi9cclxuZXhwb3J0IGZ1bmN0aW9uIGdldEdhbWVQYWNrcygpe1xyXG4gICAgbGV0IHBhY2tzID0ge307XHJcbiAgICBsZXQgX2NvbGxlY3RHYW1lUGFja3M7XHJcbiAgICBfY29sbGVjdEdhbWVQYWNrcyA9IChwYXRoOnN0cmluZyk9PntcclxuICAgICAgICBmb3IobGV0IGYgb2YgRnMucmVhZGRpclN5bmMocGF0aCkpe1xyXG4gICAgICAgICAgICBsZXQgZnBhdGggPSBQYXRoLmpvaW4ocGF0aCwgZik7XHJcbiAgICAgICAgICAgIGxldCBmc3RhdGUgPSBGcy5zdGF0U3luYyhmcGF0aCk7XHJcbiAgICAgICAgICAgIGlmKGZzdGF0ZS5pc0RpcmVjdG9yeSgpKXtcclxuICAgICAgICAgICAgICAgIGxldCBtZXRhZmlsZSA9IGZwYXRoICsgXCIubWV0YVwiO1xyXG4gICAgICAgICAgICAgICAgaWYoRnMuZXhpc3RzU3luYyhtZXRhZmlsZSkpe1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBtZXRhID0gSlNPTi5wYXJzZShGcy5yZWFkRmlsZVN5bmMobWV0YWZpbGUsIHsgZW5jb2RpbmcgOiAndXRmLTgnIH0pKTtcclxuICAgICAgICAgICAgICAgICAgICBpZihtZXRhICYmIG1ldGEuaXNCdW5kbGUpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYWNrc1ttZXRhLmJ1bmRsZU5hbWUgfHwgZl0gPSBQYXRoLm5vcm1hbGl6ZShmcGF0aCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIF9jb2xsZWN0R2FtZVBhY2tzKGZwYXRoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH07XHJcblx0cGFja3NbJ21haW4nXSA9IFBhdGgubm9ybWFsaXplKFBhdGguam9pbihFZGl0b3IuUHJvamVjdC5wYXRoLCAnYXNzZXRzJykpO1xyXG5cdF9jb2xsZWN0R2FtZVBhY2tzKFBhdGguam9pbihFZGl0b3IuUHJvamVjdC5wYXRoLCAnYXNzZXRzJykpO1xyXG4gICAgcmV0dXJuIHBhY2tzO1xyXG59XHJcbiJdfQ==
