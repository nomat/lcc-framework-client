"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.onBuildFinished = exports.onBuildStart = void 0;
const utils_1 = require("../utils");
const config_1 = require("../config");
const framework_config_1 = require("./framework-config");
const Fs = require('fs');
const Path = require('path');
const econfig = utils_1.getExtensionConfig();
/**
 * 构建开始
 */
function onBuildStart(options, callback) {
    return __awaiter(this, void 0, void 0, function* () {
        callback();
    });
}
exports.onBuildStart = onBuildStart;
/**
 * 构建完成
 */
function onBuildFinished(options, callback) {
    return __awaiter(this, void 0, void 0, function* () {
        //Editor.log(`${TAG} onBuildFinished ${JSON.stringify(options)}`);
        let pkgOptions = config_1.loadConfig().publish;
        let destJs = utils_1.findFile(options.dest, `${econfig.name}.js`, utils_1.FindFileType.FILE);
        if (destJs) {
            const VERSION = utils_1.getExtensionConfig().version;
            const tempPath = Path.join(Editor.Project.path, 'temp', econfig.name);
            if (!Fs.existsSync(tempPath)) {
                Fs.mkdirSync(tempPath, { recursive: true });
            }
            let modules = '';
            let fconfig = utils_1.getFrameworkConfig();
            if (pkgOptions.clip) {
                if (pkgOptions.clipMethod == 'auto') {
                    let pathList = [];
                    utils_1.mapDirectory(Path.join(Editor.Project.path, 'assets'), (path, file, isdir) => {
                        if (!isdir && file != `${econfig.name}.js` && file != `${econfig.name}.d.ts` &&
                            (path.endsWith('.ts') ||
                                path.endsWith('.js') ||
                                path.endsWith('.scene') ||
                                path.endsWith('.prefab'))) {
                            pathList.push(path);
                        }
                        return false;
                    });
                    const autoclipCacheConfigFile = Path.join(tempPath, 'autoclip-cache-config.json');
                    let autoclipCacheConfig = { version: VERSION, filetimes: {}, modules: "" };
                    do {
                        // 检查缓存
                        if (Fs.existsSync(autoclipCacheConfigFile)) {
                            autoclipCacheConfig = JSON.parse(Fs.readFileSync(autoclipCacheConfigFile, { encoding: 'utf-8' }));
                            if (Object.keys(autoclipCacheConfig.filetimes).length == pathList.length && autoclipCacheConfig.version == VERSION) {
                                if ((() => {
                                    for (let _path of pathList) {
                                        let pinfo = Fs.statSync(_path);
                                        if (Math.floor(pinfo.mtimeMs) != autoclipCacheConfig.filetimes[_path]) {
                                            return false;
                                        }
                                    }
                                    return true;
                                })()) {
                                    Editor.log(`${econfig.name} 使用自动裁剪缓存`);
                                    modules = autoclipCacheConfig.modules;
                                    break;
                                }
                            }
                        }
                        // 重新构建缓存
                        if (Fs.existsSync(autoclipCacheConfigFile)) {
                            Fs.unlinkSync(autoclipCacheConfigFile);
                        }
                        autoclipCacheConfig.version = VERSION;
                        autoclipCacheConfig.filetimes = {};
                        let autoclipCache = '';
                        for (let _path of pathList) {
                            let pinfo = Fs.statSync(_path);
                            autoclipCacheConfig.filetimes[_path] = Math.floor(pinfo.mtimeMs);
                            autoclipCache += Fs.readFileSync(_path, { encoding: 'utf-8' });
                        }
                        let fconfigor = new framework_config_1.FrameworkConfigurator(fconfig);
                        for (let mname in fconfig.modules) {
                            let config = fconfig.modules[mname];
                            if (config.keywords) {
                                for (let keyword of config.keywords) {
                                    let reg = new RegExp(`[^a-zA-Z_$]${keyword}[^a-zA-Z_$]`);
                                    if (reg.test(autoclipCache)) {
                                        Editor.log(`${econfig.name} KEYWORD '${keyword}' MODULE '${mname}'`);
                                        fconfigor.keepModule(mname);
                                        break;
                                    }
                                }
                            }
                        }
                        autoclipCacheConfig.modules = fconfigor.collectModules().filter((mname) => { return !fconfig.modules[mname].baseModule; }).join(',');
                        modules = autoclipCacheConfig.modules;
                        Fs.writeFileSync(autoclipCacheConfigFile, JSON.stringify(autoclipCacheConfig));
                    } while (false);
                }
                else {
                    let keepModules = pkgOptions.keepModules;
                    modules = Object.keys(fconfig.modules).filter((mname) => {
                        let module = fconfig.modules[mname];
                        let keep = keepModules[mname];
                        return !module.baseModule && (keep != null ? keep : module.keepDefault);
                    }).join(',');
                }
            }
            else {
                modules = Object.keys(fconfig.modules).filter((mname) => { return !fconfig.modules[mname].baseModule; }).join(',');
            }
            Editor.log(`${econfig.name} 模块:${modules}`);
            let debug = pkgOptions.confuse ? false : options.debug;
            const option_debug = debug ? '--debug' : '';
            const option_modules = modules ? `--modules "${modules}"` : '';
            const option_confuse = pkgOptions.confuse ? '--confuse' : '';
            const frameworkCacheConfigFile = Path.join(tempPath, 'framework-cache-config.json');
            const frameworkCacheFile = Path.join(tempPath, 'framework-cache.js');
            let frameworkCacheConfig = { version: VERSION, options: {} };
            do {
                // 检查缓存
                if (Fs.existsSync(frameworkCacheConfigFile) && Fs.existsSync(frameworkCacheFile)) {
                    frameworkCacheConfig = JSON.parse(Fs.readFileSync(frameworkCacheConfigFile, { encoding: 'utf-8' }));
                    if (frameworkCacheConfig.version == VERSION &&
                        frameworkCacheConfig.options['debug'] == option_debug &&
                        frameworkCacheConfig.options['confuse'] == option_confuse &&
                        frameworkCacheConfig.options['modules'] == option_modules) {
                        Editor.log(`${econfig.name} 使用框架缓存`);
                        break;
                    }
                }
                // 重新编译框架
                if (Fs.existsSync(frameworkCacheConfigFile)) {
                    Fs.unlinkSync(frameworkCacheConfigFile);
                }
                if (Fs.existsSync(frameworkCacheFile)) {
                    Fs.unlinkSync(frameworkCacheFile);
                }
                let command = `npx gulp buildPublishFramework --outDir "${tempPath}" --outFile framework-cache.js ${option_debug} ${option_confuse} ${option_modules}`;
                //Editor.log(`${TAG} onBuildFinished ${command}`);
                let result = yield utils_1.exeCommand(command, utils_1.EXTENSION_PATH);
                if (result.error) {
                    Editor.error('框架编译失败: ' + result.error);
                    break;
                }
                else {
                    // 写入缓存
                    frameworkCacheConfig.version = VERSION;
                    frameworkCacheConfig.options['debug'] = option_debug;
                    frameworkCacheConfig.options['confuse'] = option_confuse;
                    frameworkCacheConfig.options['modules'] = option_modules;
                    Fs.writeFileSync(frameworkCacheConfigFile, JSON.stringify(frameworkCacheConfig));
                }
            } while (false);
            if (Fs.existsSync(frameworkCacheFile)) {
                utils_1.copyFile(frameworkCacheFile, destJs);
                Editor.log(`${econfig.name} ${econfig.name}.js 替换成功`);
            }
        }
        callback();
    });
}
exports.onBuildFinished = onBuildFinished;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImJ1aWxkZXIvaG9va3MudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsb0NBQTJJO0FBQzNJLHNDQUF1QztBQUN2Qyx5REFBMEQ7QUFFMUQsTUFBTSxFQUFFLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO0FBQ3pCLE1BQU0sSUFBSSxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztBQUU3QixNQUFNLE9BQU8sR0FBRywwQkFBa0IsRUFBRSxDQUFDO0FBRXJDOztHQUVHO0FBQ0gsU0FBc0IsWUFBWSxDQUFDLE9BQW9CLEVBQUUsUUFBaUI7O1FBQ3pFLFFBQVEsRUFBRSxDQUFDO0lBQ1osQ0FBQztDQUFBO0FBRkQsb0NBRUM7QUFFRDs7R0FFRztBQUNILFNBQXNCLGVBQWUsQ0FBQyxPQUFvQixFQUFFLFFBQWlCOztRQUM1RSxrRUFBa0U7UUFDbEUsSUFBSSxVQUFVLEdBQUcsbUJBQVUsRUFBRSxDQUFDLE9BQU8sQ0FBQztRQUN0QyxJQUFJLE1BQU0sR0FBRyxnQkFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsR0FBRyxPQUFPLENBQUMsSUFBSSxLQUFLLEVBQUUsb0JBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM3RSxJQUFHLE1BQU0sRUFBQztZQUNILE1BQU0sT0FBTyxHQUFHLDBCQUFrQixFQUFFLENBQUMsT0FBTyxDQUFDO1lBQzdDLE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsTUFBTSxFQUFFLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN0RSxJQUFHLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBQztnQkFDeEIsRUFBRSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsRUFBRSxTQUFTLEVBQUcsSUFBSSxFQUFFLENBQUMsQ0FBQzthQUNoRDtZQUNQLElBQUksT0FBTyxHQUFHLEVBQUUsQ0FBQztZQUNqQixJQUFJLE9BQU8sR0FBRywwQkFBa0IsRUFBRSxDQUFDO1lBQ25DLElBQUcsVUFBVSxDQUFDLElBQUksRUFBQztnQkFDbEIsSUFBRyxVQUFVLENBQUMsVUFBVSxJQUFJLE1BQU0sRUFBQztvQkFDbEMsSUFBSSxRQUFRLEdBQVksRUFBRSxDQUFDO29CQUMzQixvQkFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBQyxFQUFFO3dCQUMzRSxJQUFHLENBQUMsS0FBSyxJQUFJLElBQUksSUFBSSxHQUFHLE9BQU8sQ0FBQyxJQUFJLEtBQUssSUFBSSxJQUFJLElBQUksR0FBRyxPQUFPLENBQUMsSUFBSSxPQUFPOzRCQUMxRSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDO2dDQUNyQixJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQztnQ0FDcEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7Z0NBQ3ZCLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBQzs0QkFDMUIsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzt5QkFDcEI7d0JBQ0QsT0FBTyxLQUFLLENBQUM7b0JBQ2QsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsTUFBTSx1QkFBdUIsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSw0QkFBNEIsQ0FBQyxDQUFDO29CQUNsRixJQUFJLG1CQUFtQixHQUF3RSxFQUFFLE9BQU8sRUFBQyxPQUFPLEVBQUUsU0FBUyxFQUFDLEVBQUUsRUFBRSxPQUFPLEVBQUMsRUFBRSxFQUFFLENBQUM7b0JBQzdJLEdBQUc7d0JBQ0YsT0FBTzt3QkFDUCxJQUFHLEVBQUUsQ0FBQyxVQUFVLENBQUMsdUJBQXVCLENBQUMsRUFBQzs0QkFDekMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsWUFBWSxDQUFDLHVCQUF1QixFQUFFLEVBQUUsUUFBUSxFQUFHLE9BQU8sRUFBRSxDQUFDLENBQUMsQ0FBQzs0QkFDbkcsSUFBRyxNQUFNLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxDQUFDLE1BQU0sSUFBSSxRQUFRLENBQUMsTUFBTSxJQUFJLG1CQUFtQixDQUFDLE9BQU8sSUFBSSxPQUFPLEVBQUM7Z0NBQ2pILElBQUcsQ0FBQyxHQUFFLEVBQUU7b0NBQ1AsS0FBSSxJQUFJLEtBQUssSUFBSSxRQUFRLEVBQUM7d0NBQ3pCLElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7d0NBQy9CLElBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksbUJBQW1CLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxFQUFDOzRDQUNwRSxPQUFPLEtBQUssQ0FBQzt5Q0FDYjtxQ0FDRDtvQ0FDRCxPQUFPLElBQUksQ0FBQztnQ0FDYixDQUFDLENBQUMsRUFBRSxFQUFDO29DQUNKLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxPQUFPLENBQUMsSUFBSSxXQUFXLENBQUMsQ0FBQztvQ0FDdkMsT0FBTyxHQUFHLG1CQUFtQixDQUFDLE9BQU8sQ0FBQztvQ0FDdEMsTUFBTTtpQ0FDTjs2QkFDRDt5QkFDRDt3QkFFRCxTQUFTO3dCQUNULElBQUcsRUFBRSxDQUFDLFVBQVUsQ0FBQyx1QkFBdUIsQ0FBQyxFQUFDOzRCQUN6QyxFQUFFLENBQUMsVUFBVSxDQUFDLHVCQUF1QixDQUFDLENBQUM7eUJBQ3ZDO3dCQUNELG1CQUFtQixDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7d0JBQ3RDLG1CQUFtQixDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7d0JBQ25DLElBQUksYUFBYSxHQUFHLEVBQUUsQ0FBQzt3QkFDdkIsS0FBSSxJQUFJLEtBQUssSUFBSSxRQUFRLEVBQUM7NEJBQ3pCLElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7NEJBQy9CLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQzs0QkFDakUsYUFBYSxJQUFJLEVBQUUsQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLEVBQUUsUUFBUSxFQUFHLE9BQU8sRUFBRSxDQUFDLENBQUM7eUJBQ2hFO3dCQUNELElBQUksU0FBUyxHQUFHLElBQUksd0NBQXFCLENBQUMsT0FBTyxDQUFDLENBQUM7d0JBQ25ELEtBQUksSUFBSSxLQUFLLElBQUksT0FBTyxDQUFDLE9BQU8sRUFBQzs0QkFDaEMsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQzs0QkFDcEMsSUFBRyxNQUFNLENBQUMsUUFBUSxFQUFDO2dDQUNsQixLQUFJLElBQUksT0FBTyxJQUFJLE1BQU0sQ0FBQyxRQUFRLEVBQUM7b0NBQ2xDLElBQUksR0FBRyxHQUFHLElBQUksTUFBTSxDQUFDLGNBQWMsT0FBTyxhQUFhLENBQUMsQ0FBQztvQ0FDekQsSUFBRyxHQUFHLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxFQUFDO3dDQUMxQixNQUFNLENBQUMsR0FBRyxDQUFDLEdBQUcsT0FBTyxDQUFDLElBQUksYUFBYSxPQUFPLGFBQWEsS0FBSyxHQUFHLENBQUMsQ0FBQzt3Q0FDckUsU0FBUyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQzt3Q0FDNUIsTUFBTTtxQ0FDTjtpQ0FDRDs2QkFDRDt5QkFDRDt3QkFDRCxtQkFBbUIsQ0FBQyxPQUFPLEdBQUcsU0FBUyxDQUFDLGNBQWMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEtBQVksRUFBQyxFQUFFLEdBQUUsT0FBTyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO3dCQUMxSSxPQUFPLEdBQUcsbUJBQW1CLENBQUMsT0FBTyxDQUFDO3dCQUN0QyxFQUFFLENBQUMsYUFBYSxDQUFDLHVCQUF1QixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDO3FCQUMvRSxRQUFNLEtBQUssRUFBRTtpQkFDZDtxQkFBSTtvQkFDSixJQUFJLFdBQVcsR0FBRyxVQUFVLENBQUMsV0FBVyxDQUFDO29CQUN6QyxPQUFPLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsS0FBWSxFQUFDLEVBQUU7d0JBQzdELElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQ3BDLElBQUksSUFBSSxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDOUIsT0FBTyxDQUFDLE1BQU0sQ0FBQyxVQUFVLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFDekUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2lCQUNiO2FBQ0Q7aUJBQUk7Z0JBQ0osT0FBTyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEtBQVksRUFBQyxFQUFFLEdBQUUsT0FBTyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQ3hIO1lBRUQsTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxJQUFJLE9BQU8sT0FBTyxFQUFFLENBQUMsQ0FBQztZQUV0QyxJQUFJLEtBQUssR0FBRyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7WUFDdkQsTUFBTSxZQUFZLEdBQUcsS0FBSyxDQUFBLENBQUMsQ0FBQSxTQUFTLENBQUEsQ0FBQyxDQUFBLEVBQUUsQ0FBQztZQUN4QyxNQUFNLGNBQWMsR0FBRyxPQUFPLENBQUEsQ0FBQyxDQUFBLGNBQWMsT0FBTyxHQUFHLENBQUEsQ0FBQyxDQUFBLEVBQUUsQ0FBQztZQUMzRCxNQUFNLGNBQWMsR0FBRyxVQUFVLENBQUMsT0FBTyxDQUFBLENBQUMsQ0FBQSxXQUFXLENBQUEsQ0FBQyxDQUFBLEVBQUUsQ0FBQztZQUV6RCxNQUFNLHdCQUF3QixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLDZCQUE2QixDQUFDLENBQUM7WUFDcEYsTUFBTSxrQkFBa0IsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxvQkFBb0IsQ0FBQyxDQUFDO1lBQ3JFLElBQUksb0JBQW9CLEdBQXFELEVBQUUsT0FBTyxFQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUMsRUFBRSxFQUFFLENBQUM7WUFDN0csR0FBRztnQkFDQyxPQUFPO2dCQUNQLElBQUcsRUFBRSxDQUFDLFVBQVUsQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxVQUFVLENBQUMsa0JBQWtCLENBQUMsRUFBQztvQkFDNUUsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsWUFBWSxDQUFDLHdCQUF3QixFQUFFLEVBQUUsUUFBUSxFQUFHLE9BQU8sRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDckcsSUFBRyxvQkFBb0IsQ0FBQyxPQUFPLElBQUksT0FBTzt3QkFDdEMsb0JBQW9CLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLFlBQVk7d0JBQ3JELG9CQUFvQixDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxjQUFjO3dCQUN6RCxvQkFBb0IsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksY0FBYyxFQUFDO3dCQUN4RSxNQUFNLENBQUMsR0FBRyxDQUFDLEdBQUcsT0FBTyxDQUFDLElBQUksU0FBUyxDQUFDLENBQUM7d0JBQ3ZCLE1BQU07cUJBQ1Q7aUJBQ0o7Z0JBRVYsU0FBUztnQkFDVCxJQUFHLEVBQUUsQ0FBQyxVQUFVLENBQUMsd0JBQXdCLENBQUMsRUFBQztvQkFDMUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO2lCQUN4QztnQkFDRCxJQUFHLEVBQUUsQ0FBQyxVQUFVLENBQUMsa0JBQWtCLENBQUMsRUFBQztvQkFDcEMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO2lCQUNsQztnQkFDUSxJQUFJLE9BQU8sR0FBRyw0Q0FBNEMsUUFBUSxrQ0FBa0MsWUFBWSxJQUFJLGNBQWMsSUFBSSxjQUFjLEVBQUUsQ0FBQztnQkFDdkosa0RBQWtEO2dCQUMzRCxJQUFJLE1BQU0sR0FBRyxNQUFNLGtCQUFVLENBQUMsT0FBTyxFQUFFLHNCQUFjLENBQUMsQ0FBQztnQkFDOUMsSUFBRyxNQUFNLENBQUMsS0FBSyxFQUFDO29CQUNaLE1BQU0sQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDeEMsTUFBTTtpQkFDVDtxQkFBSTtvQkFDRCxPQUFPO29CQUNQLG9CQUFvQixDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7b0JBQ3ZDLG9CQUFvQixDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsR0FBRyxZQUFZLENBQUM7b0JBQ3JELG9CQUFvQixDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBRyxjQUFjLENBQUM7b0JBQ3pELG9CQUFvQixDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBRyxjQUFjLENBQUM7b0JBQ3pELEVBQUUsQ0FBQyxhQUFhLENBQUMsd0JBQXdCLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUM7aUJBQ3BGO2FBQ0osUUFBTSxLQUFLLEVBQUU7WUFFZCxJQUFHLEVBQUUsQ0FBQyxVQUFVLENBQUMsa0JBQWtCLENBQUMsRUFBQztnQkFDakMsZ0JBQVEsQ0FBQyxrQkFBa0IsRUFBRSxNQUFNLENBQUMsQ0FBQztnQkFDOUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxJQUFJLElBQUksT0FBTyxDQUFDLElBQUksVUFBVSxDQUFDLENBQUM7YUFDaEQ7U0FDUDtRQUNELFFBQVEsRUFBRSxDQUFDO0lBQ1osQ0FBQztDQUFBO0FBOUlELDBDQThJQyIsImZpbGUiOiJidWlsZGVyL2hvb2tzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRVhURU5TSU9OX1BBVEgsIGV4ZUNvbW1hbmQsIGZpbmRGaWxlLCBGaW5kRmlsZVR5cGUsIGdldEV4dGVuc2lvbkNvbmZpZyxnZXRGcmFtZXdvcmtDb25maWcsbWFwRGlyZWN0b3J5LGNvcHlGaWxlIH0gZnJvbSAnLi4vdXRpbHMnO1xyXG5pbXBvcnQgeyBsb2FkQ29uZmlnIH0gZnJvbSAnLi4vY29uZmlnJztcclxuaW1wb3J0IHsgRnJhbWV3b3JrQ29uZmlndXJhdG9yIH0gZnJvbSAnLi9mcmFtZXdvcmstY29uZmlnJ1xyXG5cclxuY29uc3QgRnMgPSByZXF1aXJlKCdmcycpO1xyXG5jb25zdCBQYXRoID0gcmVxdWlyZSgncGF0aCcpO1xyXG5cclxuY29uc3QgZWNvbmZpZyA9IGdldEV4dGVuc2lvbkNvbmZpZygpO1xyXG5cclxuLyoqXHJcbiAqIOaehOW7uuW8gOWni1xyXG4gKi9cclxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIG9uQnVpbGRTdGFydChvcHRpb25zOkJ1aWxkT3B0aW9ucywgY2FsbGJhY2s6RnVuY3Rpb24pe1xyXG5cdGNhbGxiYWNrKCk7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiDmnoTlu7rlrozmiJBcclxuICovXHJcbmV4cG9ydCBhc3luYyBmdW5jdGlvbiBvbkJ1aWxkRmluaXNoZWQob3B0aW9uczpCdWlsZE9wdGlvbnMsIGNhbGxiYWNrOkZ1bmN0aW9uKXtcclxuXHQvL0VkaXRvci5sb2coYCR7VEFHfSBvbkJ1aWxkRmluaXNoZWQgJHtKU09OLnN0cmluZ2lmeShvcHRpb25zKX1gKTtcclxuXHRsZXQgcGtnT3B0aW9ucyA9IGxvYWRDb25maWcoKS5wdWJsaXNoO1xyXG5cdGxldCBkZXN0SnMgPSBmaW5kRmlsZShvcHRpb25zLmRlc3QsIGAke2Vjb25maWcubmFtZX0uanNgLCBGaW5kRmlsZVR5cGUuRklMRSk7XHJcblx0aWYoZGVzdEpzKXtcclxuICAgICAgICBjb25zdCBWRVJTSU9OID0gZ2V0RXh0ZW5zaW9uQ29uZmlnKCkudmVyc2lvbjtcclxuICAgICAgICBjb25zdCB0ZW1wUGF0aCA9IFBhdGguam9pbihFZGl0b3IuUHJvamVjdC5wYXRoLCAndGVtcCcsIGVjb25maWcubmFtZSk7XHJcbiAgICAgICAgaWYoIUZzLmV4aXN0c1N5bmModGVtcFBhdGgpKXtcclxuICAgICAgICAgICAgRnMubWtkaXJTeW5jKHRlbXBQYXRoLCB7IHJlY3Vyc2l2ZSA6IHRydWUgfSk7XHJcbiAgICAgICAgfVxyXG5cdFx0bGV0IG1vZHVsZXMgPSAnJztcclxuXHRcdGxldCBmY29uZmlnID0gZ2V0RnJhbWV3b3JrQ29uZmlnKCk7XHJcblx0XHRpZihwa2dPcHRpb25zLmNsaXApe1xyXG5cdFx0XHRpZihwa2dPcHRpb25zLmNsaXBNZXRob2QgPT0gJ2F1dG8nKXtcclxuXHRcdFx0XHRsZXQgcGF0aExpc3Q6c3RyaW5nW10gPSBbXTtcclxuXHRcdFx0XHRtYXBEaXJlY3RvcnkoUGF0aC5qb2luKEVkaXRvci5Qcm9qZWN0LnBhdGgsICdhc3NldHMnKSwgKHBhdGgsIGZpbGUsIGlzZGlyKT0+e1xyXG5cdFx0XHRcdFx0aWYoIWlzZGlyICYmIGZpbGUgIT0gYCR7ZWNvbmZpZy5uYW1lfS5qc2AgJiYgZmlsZSAhPSBgJHtlY29uZmlnLm5hbWV9LmQudHNgICYmXHJcblx0XHRcdFx0XHRcdChwYXRoLmVuZHNXaXRoKCcudHMnKSB8fCBcclxuXHRcdFx0XHRcdFx0cGF0aC5lbmRzV2l0aCgnLmpzJykgfHwgXHJcblx0XHRcdFx0XHRcdHBhdGguZW5kc1dpdGgoJy5zY2VuZScpIHx8IFxyXG5cdFx0XHRcdFx0XHRwYXRoLmVuZHNXaXRoKCcucHJlZmFiJykpKXtcclxuXHRcdFx0XHRcdFx0cGF0aExpc3QucHVzaChwYXRoKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdHJldHVybiBmYWxzZTtcclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0XHRjb25zdCBhdXRvY2xpcENhY2hlQ29uZmlnRmlsZSA9IFBhdGguam9pbih0ZW1wUGF0aCwgJ2F1dG9jbGlwLWNhY2hlLWNvbmZpZy5qc29uJyk7XHJcblx0XHRcdFx0bGV0IGF1dG9jbGlwQ2FjaGVDb25maWc6eyB2ZXJzaW9uOm51bWJlciAsIGZpbGV0aW1lczpSZWNvcmQ8c3RyaW5nLG51bWJlcj4sIG1vZHVsZXM6c3RyaW5nIH0gPSB7IHZlcnNpb246VkVSU0lPTiwgZmlsZXRpbWVzOnt9LCBtb2R1bGVzOlwiXCIgfTtcclxuXHRcdFx0XHRkbyB7XHJcblx0XHRcdFx0XHQvLyDmo4Dmn6XnvJPlrZhcclxuXHRcdFx0XHRcdGlmKEZzLmV4aXN0c1N5bmMoYXV0b2NsaXBDYWNoZUNvbmZpZ0ZpbGUpKXtcclxuXHRcdFx0XHRcdFx0YXV0b2NsaXBDYWNoZUNvbmZpZyA9IEpTT04ucGFyc2UoRnMucmVhZEZpbGVTeW5jKGF1dG9jbGlwQ2FjaGVDb25maWdGaWxlLCB7IGVuY29kaW5nIDogJ3V0Zi04JyB9KSk7XHJcblx0XHRcdFx0XHRcdGlmKE9iamVjdC5rZXlzKGF1dG9jbGlwQ2FjaGVDb25maWcuZmlsZXRpbWVzKS5sZW5ndGggPT0gcGF0aExpc3QubGVuZ3RoICYmIGF1dG9jbGlwQ2FjaGVDb25maWcudmVyc2lvbiA9PSBWRVJTSU9OKXtcclxuXHRcdFx0XHRcdFx0XHRpZigoKCk9PntcclxuXHRcdFx0XHRcdFx0XHRcdGZvcihsZXQgX3BhdGggb2YgcGF0aExpc3Qpe1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRsZXQgcGluZm8gPSBGcy5zdGF0U3luYyhfcGF0aCk7XHJcblx0XHRcdFx0XHRcdFx0XHRcdGlmKE1hdGguZmxvb3IocGluZm8ubXRpbWVNcykgIT0gYXV0b2NsaXBDYWNoZUNvbmZpZy5maWxldGltZXNbX3BhdGhdKXtcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRyZXR1cm4gZmFsc2U7XHJcblx0XHRcdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHRcdHJldHVybiB0cnVlO1xyXG5cdFx0XHRcdFx0XHRcdH0pKCkpe1xyXG5cdFx0XHRcdFx0XHRcdFx0RWRpdG9yLmxvZyhgJHtlY29uZmlnLm5hbWV9IOS9v+eUqOiHquWKqOijgeWJque8k+WtmGApO1xyXG5cdFx0XHRcdFx0XHRcdFx0bW9kdWxlcyA9IGF1dG9jbGlwQ2FjaGVDb25maWcubW9kdWxlcztcclxuXHRcdFx0XHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRcdC8vIOmHjeaWsOaehOW7uue8k+WtmFxyXG5cdFx0XHRcdFx0aWYoRnMuZXhpc3RzU3luYyhhdXRvY2xpcENhY2hlQ29uZmlnRmlsZSkpe1xyXG5cdFx0XHRcdFx0XHRGcy51bmxpbmtTeW5jKGF1dG9jbGlwQ2FjaGVDb25maWdGaWxlKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdGF1dG9jbGlwQ2FjaGVDb25maWcudmVyc2lvbiA9IFZFUlNJT047XHJcblx0XHRcdFx0XHRhdXRvY2xpcENhY2hlQ29uZmlnLmZpbGV0aW1lcyA9IHt9O1xyXG5cdFx0XHRcdFx0bGV0IGF1dG9jbGlwQ2FjaGUgPSAnJztcclxuXHRcdFx0XHRcdGZvcihsZXQgX3BhdGggb2YgcGF0aExpc3Qpe1xyXG5cdFx0XHRcdFx0XHRsZXQgcGluZm8gPSBGcy5zdGF0U3luYyhfcGF0aCk7XHJcblx0XHRcdFx0XHRcdGF1dG9jbGlwQ2FjaGVDb25maWcuZmlsZXRpbWVzW19wYXRoXSA9IE1hdGguZmxvb3IocGluZm8ubXRpbWVNcyk7XHJcblx0XHRcdFx0XHRcdGF1dG9jbGlwQ2FjaGUgKz0gRnMucmVhZEZpbGVTeW5jKF9wYXRoLCB7IGVuY29kaW5nIDogJ3V0Zi04JyB9KTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdGxldCBmY29uZmlnb3IgPSBuZXcgRnJhbWV3b3JrQ29uZmlndXJhdG9yKGZjb25maWcpO1xyXG5cdFx0XHRcdFx0Zm9yKGxldCBtbmFtZSBpbiBmY29uZmlnLm1vZHVsZXMpe1xyXG5cdFx0XHRcdFx0XHRsZXQgY29uZmlnID0gZmNvbmZpZy5tb2R1bGVzW21uYW1lXTtcclxuXHRcdFx0XHRcdFx0aWYoY29uZmlnLmtleXdvcmRzKXtcclxuXHRcdFx0XHRcdFx0XHRmb3IobGV0IGtleXdvcmQgb2YgY29uZmlnLmtleXdvcmRzKXtcclxuXHRcdFx0XHRcdFx0XHRcdGxldCByZWcgPSBuZXcgUmVnRXhwKGBbXmEtekEtWl8kXSR7a2V5d29yZH1bXmEtekEtWl8kXWApO1xyXG5cdFx0XHRcdFx0XHRcdFx0aWYocmVnLnRlc3QoYXV0b2NsaXBDYWNoZSkpe1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRFZGl0b3IubG9nKGAke2Vjb25maWcubmFtZX0gS0VZV09SRCAnJHtrZXl3b3JkfScgTU9EVUxFICcke21uYW1lfSdgKTtcclxuXHRcdFx0XHRcdFx0XHRcdFx0ZmNvbmZpZ29yLmtlZXBNb2R1bGUobW5hbWUpO1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdGF1dG9jbGlwQ2FjaGVDb25maWcubW9kdWxlcyA9IGZjb25maWdvci5jb2xsZWN0TW9kdWxlcygpLmZpbHRlcigobW5hbWU6c3RyaW5nKT0+eyByZXR1cm4gIWZjb25maWcubW9kdWxlc1ttbmFtZV0uYmFzZU1vZHVsZTsgfSkuam9pbignLCcpO1xyXG5cdFx0XHRcdFx0bW9kdWxlcyA9IGF1dG9jbGlwQ2FjaGVDb25maWcubW9kdWxlcztcclxuXHRcdFx0XHRcdEZzLndyaXRlRmlsZVN5bmMoYXV0b2NsaXBDYWNoZUNvbmZpZ0ZpbGUsIEpTT04uc3RyaW5naWZ5KGF1dG9jbGlwQ2FjaGVDb25maWcpKTtcclxuXHRcdFx0XHR9d2hpbGUoZmFsc2UpO1xyXG5cdFx0XHR9ZWxzZXtcclxuXHRcdFx0XHRsZXQga2VlcE1vZHVsZXMgPSBwa2dPcHRpb25zLmtlZXBNb2R1bGVzO1xyXG5cdFx0XHRcdG1vZHVsZXMgPSBPYmplY3Qua2V5cyhmY29uZmlnLm1vZHVsZXMpLmZpbHRlcigobW5hbWU6c3RyaW5nKT0+eyBcclxuXHRcdFx0XHRcdGxldCBtb2R1bGUgPSBmY29uZmlnLm1vZHVsZXNbbW5hbWVdO1xyXG5cdFx0XHRcdFx0bGV0IGtlZXAgPSBrZWVwTW9kdWxlc1ttbmFtZV07XHJcblx0XHRcdFx0XHRyZXR1cm4gIW1vZHVsZS5iYXNlTW9kdWxlICYmIChrZWVwICE9IG51bGwgPyBrZWVwIDogbW9kdWxlLmtlZXBEZWZhdWx0KTtcclxuXHRcdFx0XHR9KS5qb2luKCcsJyk7XHJcblx0XHRcdH1cclxuXHRcdH1lbHNle1xyXG5cdFx0XHRtb2R1bGVzID0gT2JqZWN0LmtleXMoZmNvbmZpZy5tb2R1bGVzKS5maWx0ZXIoKG1uYW1lOnN0cmluZyk9PnsgcmV0dXJuICFmY29uZmlnLm1vZHVsZXNbbW5hbWVdLmJhc2VNb2R1bGU7IH0pLmpvaW4oJywnKTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0RWRpdG9yLmxvZyhgJHtlY29uZmlnLm5hbWV9IOaooeWdlzoke21vZHVsZXN9YCk7XHJcbiAgICAgICAgXHJcbiAgICAgICAgbGV0IGRlYnVnID0gcGtnT3B0aW9ucy5jb25mdXNlID8gZmFsc2UgOiBvcHRpb25zLmRlYnVnO1xyXG4gICAgICAgIGNvbnN0IG9wdGlvbl9kZWJ1ZyA9IGRlYnVnPyctLWRlYnVnJzonJztcclxuICAgICAgICBjb25zdCBvcHRpb25fbW9kdWxlcyA9IG1vZHVsZXM/YC0tbW9kdWxlcyBcIiR7bW9kdWxlc31cImA6Jyc7XHJcbiAgICAgICAgY29uc3Qgb3B0aW9uX2NvbmZ1c2UgPSBwa2dPcHRpb25zLmNvbmZ1c2U/Jy0tY29uZnVzZSc6Jyc7XHJcblxyXG4gICAgICAgIGNvbnN0IGZyYW1ld29ya0NhY2hlQ29uZmlnRmlsZSA9IFBhdGguam9pbih0ZW1wUGF0aCwgJ2ZyYW1ld29yay1jYWNoZS1jb25maWcuanNvbicpO1xyXG4gICAgICAgIGNvbnN0IGZyYW1ld29ya0NhY2hlRmlsZSA9IFBhdGguam9pbih0ZW1wUGF0aCwgJ2ZyYW1ld29yay1jYWNoZS5qcycpO1xyXG4gICAgICAgIGxldCBmcmFtZXdvcmtDYWNoZUNvbmZpZzp7IHZlcnNpb246bnVtYmVyICxvcHRpb25zOlJlY29yZDxzdHJpbmcsc3RyaW5nPiB9ID0geyB2ZXJzaW9uOlZFUlNJT04sIG9wdGlvbnM6e30gfTtcclxuICAgICAgICBkbyB7XHJcbiAgICAgICAgICAgIC8vIOajgOafpee8k+WtmFxyXG4gICAgICAgICAgICBpZihGcy5leGlzdHNTeW5jKGZyYW1ld29ya0NhY2hlQ29uZmlnRmlsZSkgJiYgRnMuZXhpc3RzU3luYyhmcmFtZXdvcmtDYWNoZUZpbGUpKXtcclxuICAgICAgICAgICAgICAgIGZyYW1ld29ya0NhY2hlQ29uZmlnID0gSlNPTi5wYXJzZShGcy5yZWFkRmlsZVN5bmMoZnJhbWV3b3JrQ2FjaGVDb25maWdGaWxlLCB7IGVuY29kaW5nIDogJ3V0Zi04JyB9KSk7XHJcbiAgICAgICAgICAgICAgICBpZihmcmFtZXdvcmtDYWNoZUNvbmZpZy52ZXJzaW9uID09IFZFUlNJT04gJiYgXHJcbiAgICAgICAgICAgICAgICAgICAgZnJhbWV3b3JrQ2FjaGVDb25maWcub3B0aW9uc1snZGVidWcnXSA9PSBvcHRpb25fZGVidWcgJiYgXHJcbiAgICAgICAgICAgICAgICAgICAgZnJhbWV3b3JrQ2FjaGVDb25maWcub3B0aW9uc1snY29uZnVzZSddID09IG9wdGlvbl9jb25mdXNlICYmIFxyXG4gICAgICAgICAgICAgICAgICAgIGZyYW1ld29ya0NhY2hlQ29uZmlnLm9wdGlvbnNbJ21vZHVsZXMnXSA9PSBvcHRpb25fbW9kdWxlcyl7XHJcblx0XHRcdFx0XHRcdEVkaXRvci5sb2coYCR7ZWNvbmZpZy5uYW1lfSDkvb/nlKjmoYbmnrbnvJPlrZhgKTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuXHRcdFx0Ly8g6YeN5paw57yW6K+R5qGG5p62XHJcblx0XHRcdGlmKEZzLmV4aXN0c1N5bmMoZnJhbWV3b3JrQ2FjaGVDb25maWdGaWxlKSl7XHJcblx0XHRcdFx0RnMudW5saW5rU3luYyhmcmFtZXdvcmtDYWNoZUNvbmZpZ0ZpbGUpO1xyXG5cdFx0XHR9XHJcblx0XHRcdGlmKEZzLmV4aXN0c1N5bmMoZnJhbWV3b3JrQ2FjaGVGaWxlKSl7XHJcblx0XHRcdFx0RnMudW5saW5rU3luYyhmcmFtZXdvcmtDYWNoZUZpbGUpO1xyXG5cdFx0XHR9XHJcbiAgICAgICAgICAgIGxldCBjb21tYW5kID0gYG5weCBndWxwIGJ1aWxkUHVibGlzaEZyYW1ld29yayAtLW91dERpciBcIiR7dGVtcFBhdGh9XCIgLS1vdXRGaWxlIGZyYW1ld29yay1jYWNoZS5qcyAke29wdGlvbl9kZWJ1Z30gJHtvcHRpb25fY29uZnVzZX0gJHtvcHRpb25fbW9kdWxlc31gO1xyXG4gICAgICAgICAgICAvL0VkaXRvci5sb2coYCR7VEFHfSBvbkJ1aWxkRmluaXNoZWQgJHtjb21tYW5kfWApO1xyXG5cdFx0XHRsZXQgcmVzdWx0ID0gYXdhaXQgZXhlQ29tbWFuZChjb21tYW5kLCBFWFRFTlNJT05fUEFUSCk7XHJcbiAgICAgICAgICAgIGlmKHJlc3VsdC5lcnJvcil7XHJcbiAgICAgICAgICAgICAgICBFZGl0b3IuZXJyb3IoJ+ahhuaetue8luivkeWksei0pTogJyArIHJlc3VsdC5lcnJvcik7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICAvLyDlhpnlhaXnvJPlrZhcclxuICAgICAgICAgICAgICAgIGZyYW1ld29ya0NhY2hlQ29uZmlnLnZlcnNpb24gPSBWRVJTSU9OO1xyXG4gICAgICAgICAgICAgICAgZnJhbWV3b3JrQ2FjaGVDb25maWcub3B0aW9uc1snZGVidWcnXSA9IG9wdGlvbl9kZWJ1ZztcclxuICAgICAgICAgICAgICAgIGZyYW1ld29ya0NhY2hlQ29uZmlnLm9wdGlvbnNbJ2NvbmZ1c2UnXSA9IG9wdGlvbl9jb25mdXNlO1xyXG4gICAgICAgICAgICAgICAgZnJhbWV3b3JrQ2FjaGVDb25maWcub3B0aW9uc1snbW9kdWxlcyddID0gb3B0aW9uX21vZHVsZXM7XHJcbiAgICAgICAgICAgICAgICBGcy53cml0ZUZpbGVTeW5jKGZyYW1ld29ya0NhY2hlQ29uZmlnRmlsZSwgSlNPTi5zdHJpbmdpZnkoZnJhbWV3b3JrQ2FjaGVDb25maWcpKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH13aGlsZShmYWxzZSk7XHJcblxyXG4gICAgICAgIGlmKEZzLmV4aXN0c1N5bmMoZnJhbWV3b3JrQ2FjaGVGaWxlKSl7XHJcbiAgICAgICAgICAgIGNvcHlGaWxlKGZyYW1ld29ya0NhY2hlRmlsZSwgZGVzdEpzKTtcclxuXHRcdFx0RWRpdG9yLmxvZyhgJHtlY29uZmlnLm5hbWV9ICR7ZWNvbmZpZy5uYW1lfS5qcyDmm7/mjaLmiJDlip9gKTtcclxuICAgICAgICB9XHJcblx0fVxyXG5cdGNhbGxiYWNrKCk7XHJcbn1cclxuIl19
