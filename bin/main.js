"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.messages = exports.unload = exports.load = void 0;
const utils_1 = require("./utils");
const hooks_1 = require("./builder/hooks");
const Path = require('path');
const econfig = utils_1.getExtensionConfig();
/**
 * 导入框架
 */
function importFramework() {
    let sources = Path.join(utils_1.EXTENSION_PATH, 'assets');
    let target = `assets/${econfig.name}/`;
    // 如果已经导入寻找目标目录
    let path = utils_1.findFile(Path.join(Editor.Project.path, 'assets'), econfig.name, utils_1.FindFileType.DIRECTORY);
    if (path) {
        target = path.substring(path.indexOf("assets")).replace("\\", "/");
    }
    utils_1.copyDirectory(sources, Path.join(Editor.Project.path, target));
    Editor.assetdb.refresh('db://' + target);
}
/**
 * 插件加载回调
 */
function load() {
    Editor.Builder.on('build-start', hooks_1.onBuildStart);
    Editor.Builder.on('build-finished', hooks_1.onBuildFinished);
}
exports.load = load;
/**
 * 插件卸载回调
 */
function unload() {
    Editor.Builder.removeListener('build-start', hooks_1.onBuildStart);
    Editor.Builder.removeListener('build-finished', hooks_1.onBuildFinished);
}
exports.unload = unload;
/**
 * 消息合集
 */
var messages;
(function (messages) {
    /**
     * 导入或更新框架
     */
    function import_framework() {
        importFramework();
    }
    messages.import_framework = import_framework;
    /**
     * 功能界面
     */
    function function_panel() {
        Editor.Panel.open(econfig.name);
    }
    messages.function_panel = function_panel;
})(messages = exports.messages || (exports.messages = {}));
;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQ0EsbUNBQW9HO0FBQ3BHLDJDQUErRDtBQUUvRCxNQUFNLElBQUksR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7QUFFN0IsTUFBTSxPQUFPLEdBQUcsMEJBQWtCLEVBQUUsQ0FBQztBQUVyQzs7R0FFRztBQUNILFNBQVMsZUFBZTtJQUN2QixJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLHNCQUFjLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDL0MsSUFBSSxNQUFNLEdBQUcsVUFBVSxPQUFPLENBQUMsSUFBSSxHQUFHLENBQUM7SUFFMUMsZUFBZTtJQUNmLElBQUksSUFBSSxHQUFHLGdCQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsRUFBRSxPQUFPLENBQUMsSUFBSSxFQUFFLG9CQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDcEcsSUFBRyxJQUFJLEVBQUM7UUFDUCxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksRUFBQyxHQUFHLENBQUMsQ0FBQztLQUNsRTtJQUNELHFCQUFhLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQztJQUMvRCxNQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLENBQUM7QUFDMUMsQ0FBQztBQUVEOztHQUVHO0FBQ0gsU0FBZ0IsSUFBSTtJQUNuQixNQUFNLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxhQUFhLEVBQUUsb0JBQVksQ0FBQyxDQUFDO0lBQzVDLE1BQU0sQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLGdCQUFnQixFQUFFLHVCQUFlLENBQUMsQ0FBQztBQUN6RCxDQUFDO0FBSEQsb0JBR0M7QUFFRDs7R0FFRztBQUNILFNBQWdCLE1BQU07SUFDckIsTUFBTSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsYUFBYSxFQUFFLG9CQUFZLENBQUMsQ0FBQztJQUN4RCxNQUFNLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsRUFBRSx1QkFBZSxDQUFDLENBQUM7QUFDckUsQ0FBQztBQUhELHdCQUdDO0FBRUQ7O0dBRUc7QUFDSCxJQUFpQixRQUFRLENBY3hCO0FBZEQsV0FBaUIsUUFBUTtJQUN4Qjs7T0FFRztJQUNILFNBQWdCLGdCQUFnQjtRQUMvQixlQUFlLEVBQUUsQ0FBQztJQUNuQixDQUFDO0lBRmUseUJBQWdCLG1CQUUvQixDQUFBO0lBRUU7O09BRUc7SUFDSCxTQUFnQixjQUFjO1FBQzFCLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBRmUsdUJBQWMsaUJBRTdCLENBQUE7QUFDTCxDQUFDLEVBZGdCLFFBQVEsR0FBUixnQkFBUSxLQUFSLGdCQUFRLFFBY3hCO0FBQUEsQ0FBQyIsImZpbGUiOiJtYWluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbmltcG9ydCB7IEVYVEVOU0lPTl9QQVRILCBmaW5kRmlsZSwgRmluZEZpbGVUeXBlLCBjb3B5RGlyZWN0b3J5LCBnZXRFeHRlbnNpb25Db25maWcgfSBmcm9tICcuL3V0aWxzJztcclxuaW1wb3J0IHsgb25CdWlsZFN0YXJ0LCBvbkJ1aWxkRmluaXNoZWQgfSBmcm9tICcuL2J1aWxkZXIvaG9va3MnXHJcblxyXG5jb25zdCBQYXRoID0gcmVxdWlyZSgncGF0aCcpO1xyXG5cclxuY29uc3QgZWNvbmZpZyA9IGdldEV4dGVuc2lvbkNvbmZpZygpO1xyXG5cclxuLyoqXHJcbiAqIOWvvOWFpeahhuaetlxyXG4gKi9cclxuZnVuY3Rpb24gaW1wb3J0RnJhbWV3b3JrKCkge1xyXG5cdGxldCBzb3VyY2VzID0gUGF0aC5qb2luKEVYVEVOU0lPTl9QQVRILCAnYXNzZXRzJyk7XHJcbiAgICBsZXQgdGFyZ2V0ID0gYGFzc2V0cy8ke2Vjb25maWcubmFtZX0vYDtcclxuICAgIFxyXG5cdC8vIOWmguaenOW3sue7j+WvvOWFpeWvu+aJvuebruagh+ebruW9lVxyXG5cdGxldCBwYXRoID0gZmluZEZpbGUoUGF0aC5qb2luKEVkaXRvci5Qcm9qZWN0LnBhdGgsICdhc3NldHMnKSwgZWNvbmZpZy5uYW1lLCBGaW5kRmlsZVR5cGUuRElSRUNUT1JZKTtcclxuXHRpZihwYXRoKXtcclxuXHRcdHRhcmdldCA9IHBhdGguc3Vic3RyaW5nKHBhdGguaW5kZXhPZihcImFzc2V0c1wiKSkucmVwbGFjZShcIlxcXFxcIixcIi9cIik7XHJcblx0fVxyXG5cdGNvcHlEaXJlY3Rvcnkoc291cmNlcywgUGF0aC5qb2luKEVkaXRvci5Qcm9qZWN0LnBhdGgsIHRhcmdldCkpO1xyXG5cdEVkaXRvci5hc3NldGRiLnJlZnJlc2goJ2RiOi8vJyArIHRhcmdldCk7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiDmj5Lku7bliqDovb3lm57osINcclxuICovXHJcbmV4cG9ydCBmdW5jdGlvbiBsb2FkKCl7XHJcblx0RWRpdG9yLkJ1aWxkZXIub24oJ2J1aWxkLXN0YXJ0Jywgb25CdWlsZFN0YXJ0KTtcclxuICAgIEVkaXRvci5CdWlsZGVyLm9uKCdidWlsZC1maW5pc2hlZCcsIG9uQnVpbGRGaW5pc2hlZCk7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiDmj5Lku7bljbjovb3lm57osINcclxuICovXHJcbmV4cG9ydCBmdW5jdGlvbiB1bmxvYWQoKXtcclxuXHRFZGl0b3IuQnVpbGRlci5yZW1vdmVMaXN0ZW5lcignYnVpbGQtc3RhcnQnLCBvbkJ1aWxkU3RhcnQpO1xyXG4gICAgRWRpdG9yLkJ1aWxkZXIucmVtb3ZlTGlzdGVuZXIoJ2J1aWxkLWZpbmlzaGVkJywgb25CdWlsZEZpbmlzaGVkKTtcclxufVxyXG5cclxuLyoqXHJcbiAqIOa2iOaBr+WQiOmbhlxyXG4gKi9cclxuZXhwb3J0IG5hbWVzcGFjZSBtZXNzYWdlcyB7XHJcblx0LyoqXHJcblx0ICog5a+85YWl5oiW5pu05paw5qGG5p62XHJcblx0ICovXHJcblx0ZXhwb3J0IGZ1bmN0aW9uIGltcG9ydF9mcmFtZXdvcmsoKXtcclxuXHRcdGltcG9ydEZyYW1ld29yaygpO1xyXG5cdH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIOWKn+iDveeVjOmdolxyXG4gICAgICovXHJcbiAgICBleHBvcnQgZnVuY3Rpb24gZnVuY3Rpb25fcGFuZWwoKXtcclxuICAgICAgICBFZGl0b3IuUGFuZWwub3BlbihlY29uZmlnLm5hbWUpO1xyXG4gICAgfVxyXG59O1xyXG4iXX0=
