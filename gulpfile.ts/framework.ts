import { FrameworkConfigurator } from './framework-config';
import { EXTENSION_PATH, getExtensionConfig } from './utils';

const gulp = require("gulp");
const ts = require("gulp-typescript");
const sourcemaps = require('gulp-sourcemaps');
const rename = require("gulp-rename");
const uglify = require("gulp-uglify");
const concat = require('gulp-concat');
const clean = require('gulp-clean');
const jsobfuscator = require('gulp-javascript-obfuscator');
const debug = require('gulp-debug');
const notify = require('gulp-notify');
const browserify = require('gulp-browserify');
const merge = require('merge2');
const through = require('through2');
const fs = require("fs");
const path = require("path");
const yargs = require('yargs');

const econfig = getExtensionConfig();

/**
 * 清空资源框架
 */
function cleanAssetFramework(cb) {
	if(fs.existsSync(`${EXTENSION_PATH}/framework`)){
		console.log("cleanAssetFramework");
		return gulp.src([ `${EXTENSION_PATH}/assets/scripts/${econfig.name}.js`, `${EXTENSION_PATH}/assets/scripts/${econfig.name}.d.ts` ], { read: false, allowEmpty:true })
			.pipe(clean({ force:true }));
	}else{
		console.error("framework not found!");
		cb();
	}
}

/**
 * 构建资源框架
 */
function buildAssetFramework(cb) {
	if(fs.existsSync(`${EXTENSION_PATH}/framework`)){
		console.log("buildAssetFramework");
		let tsResult = gulp.src([ `${EXTENSION_PATH}/framework/src/**/*.ts`, `${EXTENSION_PATH}/@types/creator.d.ts` ])
			.pipe(sourcemaps.init())
            .pipe(ts.createProject(`${EXTENSION_PATH}/framework/tsconfig.json`, {
				target : "ES5",
            })())
			.on("error",function(err:any){
				console.error(err.message);
			});
		let fconfigor = new FrameworkConfigurator(`${EXTENSION_PATH}/framework/config.json`);
		fconfigor.keepAllModules();
		return merge(
			tsResult.js
				.pipe(browserify({
					insertGlobals : true,
					debug : true,
				}))
				.pipe(through.obj(function (chunk, enc, callback){
                    let sdata = chunk.contents.toString();
					for(let lib of fconfigor.collectLibs()){
						let libstr = fs.readFileSync(lib, 'utf8');
						sdata = sdata + '\n' + libstr;
					}
                    chunk.contents = Buffer.from(sdata);
                    this.push(chunk)
                    callback();
                }))
				.pipe(sourcemaps.write())
				.pipe(gulp.dest(`${EXTENSION_PATH}/assets/scripts`)),
			tsResult.dts
				.pipe(through.obj(function (chunk, enc, callback){
                    let sdata = chunk.contents.toString();
					for(let tp of fconfigor.collectTypes()){
						let tpstr = fs.readFileSync(tp, 'utf8');
						sdata = sdata + '\n' + tpstr;
					}
                    chunk.contents = Buffer.from(sdata);
                    this.push(chunk)
                    callback();
                }))
				.pipe(gulp.dest(`${EXTENSION_PATH}/assets/scripts`))
			);
	}else{
		console.error("framework not found!");
		cb();
	}
}

/**
 * 构建发布框架
 */
function buildPublishFramework(cb) {
	if(fs.existsSync(`${EXTENSION_PATH}/framework`)){
		console.log("buildPublishFramework");
		let argv = yargs
			.default('debug', false)
			.default('confuse', false)
			.default('modules', "base")
			.default('outDir', "temp")
			.default('outFile', "framework-cache.js")
			.boolean(['debug', 'confuse'])
			.argv;
        
        // 计算编译源文件
        let fconfigor = new FrameworkConfigurator(`${EXTENSION_PATH}/framework/config.json`);
        fconfigor.keepModules(argv.modules.split(','));
		let modules = fconfigor.collectModules();
		console.log(`KeepModules : ${modules.join(',')}`);
        let sources = fconfigor.collectSources().map((item, index, input) => { return `${EXTENSION_PATH}/framework/src/${item}`; });
        sources.push(...[ `${EXTENSION_PATH}/@types/creator.d.ts` ]);
        
		let gulpState = gulp.src(sources);
		if(argv.debug){
			gulpState = gulpState.pipe(sourcemaps.init());
		}
		gulpState = gulpState.pipe(ts.createProject(`${EXTENSION_PATH}/framework/tsconfig.json`, { 
			target : "ES5",
			declaration : false,
			removeComments : true,
			outFile : argv.outFile,
		})())
		.on("error",function(err:any){
			console.error(err.message);
		});
		gulpState = gulpState.js;
		gulpState = gulpState.pipe(browserify({
			insertGlobals : true,
			debug : false,
		}))
		gulpState = gulpState.pipe(through.obj(function (chunk, enc, callback){
			let sdata = chunk.contents.toString();
			for(let lib of fconfigor.collectLibs()){
				let libstr = fs.readFileSync(lib, 'utf8');
				sdata = sdata + '\n' + libstr;
			}
			chunk.contents = Buffer.from(sdata);
			this.push(chunk)
			callback();
		}));
		if(argv.confuse){
			gulpState = gulpState.pipe(jsobfuscator({
				compact: true
			}));
		}
		gulpState = gulpState.pipe(uglify({
            mangle: true,//类型：Boolean 默认：true 是否修改变量名
            compress: false,//类型：Boolean 默认：true 是否完全压缩
        }));
		if(argv.debug){
			gulpState = gulpState.pipe(sourcemaps.write());
		}
		return gulpState.pipe(gulp.dest(argv.outDir));
	}else{
		console.error("framework not found!");
		cb();
	}
}

export const buildAsset = gulp.series(cleanAssetFramework, buildAssetFramework);

export const buildPublish = buildPublishFramework;
