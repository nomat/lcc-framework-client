import { getGamePacks, findFile, FindFileType, EXTENSION_PATH, getExtensionConfig  } from './utils'

const gulp = require("gulp");
const ts = require("gulp-typescript");
const sourcemaps = require('gulp-sourcemaps');
const rename = require("gulp-rename");
const uglify = require("gulp-uglify");
const concat = require('gulp-concat');
const clean = require('gulp-clean');
const jsobfuscator = require('gulp-javascript-obfuscator');
const debug = require('gulp-debug');
const notify = require('gulp-notify');
const merge = require('merge2');
const through = require('through2');
const yargs = require('yargs');
const FS = require("fs");
const Path = require("path");

const econfig = getExtensionConfig();

/**
 * 收集TS文件
 */
function collectTsFiles(files:string[], path:string, excPaths:string[]){
	for(let f of FS.readdirSync(path)){
		let fpath:string = Path.normalize(Path.join(path, f));
		let fstate = FS.statSync(fpath);
		if(fstate.isDirectory() && excPaths.indexOf(fpath) == -1){
			collectTsFiles(files, fpath, excPaths);
		}else if(fpath.endsWith('.ts')){
			files.push(fpath);
		}
	}
}

/**
 * 构建包声明
 */
export function buildDeclaration(cb) {
	let argv = yargs
		.default('pack', 'main')
		.default('project', '../../')
		.argv;
	let files:string[] = [ 
		`${argv.project}/creator.d.ts`,
		`${argv.project}/@types/**/*.ts`, 
		`!${argv.project}/@types/packs/${argv.pack}.d.ts`, 
	];
	let fwpath = findFile(Path.join(argv.project, 'assets'), econfig.name, FindFileType.DIRECTORY);
	if(fwpath){
		files.push(`${fwpath}/**/*.ts`);
	}
	let gamePacks = getGamePacks(argv.project);
	let packPaths = [];
	for(let p in gamePacks){
		packPaths.push(gamePacks[p]);
	} 
	collectTsFiles(files, gamePacks[argv.pack], packPaths);
	let sources = gulp.src(files);//.pipe(debug({title: 'file:'}));
	console.log("buildDeclaration " + argv.pack);
	
	//let filecount:{[key:string]:number} = {};
	let tsResult = sources.pipe(ts.createProject({
		module : "commonjs",
		target : "ES5",
		experimentalDecorators : true,
		skipLibCheck : true,
		noImplicitAny : false,
		declaration : true,
		removeComments : false,
		rootDir : argv.project
	})())
	.on("error",function(err:any){
		console.error(err.message);
	});
	return tsResult.dts
		/*
		.pipe(through.obj(function (chunk, enc, callback){
			let basename = path.basename(chunk.path, ".d.ts")
			let count = filecount[basename] || 0;
			filecount[basename] = count + 1;
			if(count > 0){
				basename = basename + "_" + count;
			}
			let sdata = chunk.contents.toString();
			sdata = "declare module " + basename + "{\n" + sdata + "\n}\n";
			chunk.contents = Buffer.from(sdata);
			this.push(chunk)
			callback();
		}))
		*/
		.pipe(concat(`${argv.pack}.d.ts`))
		.pipe(through.obj(function (chunk, enc, callback){
			let sdata = chunk.contents.toString();
			sdata = `
// generate by ${econfig.name}

declare namespace lcc$PACKS.${argv.pack.replace('-','_')} {

${sdata.replace(/([ \t]+(default|declare)[ \t]+)+/g, " ")}
}
			`;
			chunk.contents = Buffer.from(sdata);
			this.push(chunk)
			callback();
		}))
		.pipe(gulp.dest(Path.join(argv.project, '@types', 'packs')));
}
